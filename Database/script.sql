USE [master]
GO
/****** Object:  Database [SmartLicenseDB]    Script Date: 10/19/2020 11:54:28 PM ******/
CREATE DATABASE [SmartLicenseDB]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'SmartLicenseDB', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SmartLicenseDB.mdf' , SIZE = 73728KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'SmartLicenseDB_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.MSSQLSERVER\MSSQL\DATA\SmartLicenseDB_log.ldf' , SIZE = 73728KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [SmartLicenseDB] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SmartLicenseDB].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [SmartLicenseDB] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET ARITHABORT OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [SmartLicenseDB] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [SmartLicenseDB] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET  DISABLE_BROKER 
GO
ALTER DATABASE [SmartLicenseDB] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [SmartLicenseDB] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET RECOVERY FULL 
GO
ALTER DATABASE [SmartLicenseDB] SET  MULTI_USER 
GO
ALTER DATABASE [SmartLicenseDB] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [SmartLicenseDB] SET DB_CHAINING OFF 
GO
ALTER DATABASE [SmartLicenseDB] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [SmartLicenseDB] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [SmartLicenseDB] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'SmartLicenseDB', N'ON'
GO
ALTER DATABASE [SmartLicenseDB] SET QUERY_STORE = OFF
GO
USE [SmartLicenseDB]
GO
/****** Object:  Table [dbo].[Apply]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Apply](
	[ApplyId] [int] IDENTITY(1,1) NOT NULL,
	[Username] [varchar](150) NOT NULL,
	[LicenseType] [varchar](50) NOT NULL,
	[ApplyFor] [varchar](50) NOT NULL,
	[Payment] [varchar](50) NOT NULL,
	[TestDate] [varchar](50) NOT NULL,
	[IsIssued] [varchar](50) NOT NULL,
	[UtilityImage] [varbinary](max) NOT NULL,
	[DocumentImage] [varbinary](max) NOT NULL,
 CONSTRAINT [PK_Apply] PRIMARY KEY CLUSTERED 
(
	[ApplyId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Bkash]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Bkash](
	[Number] [varchar](50) NOT NULL,
	[Pin] [varchar](50) NOT NULL,
	[Amount] [int] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DrivingLicense]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DrivingLicense](
	[DrivingLicenseId] [varchar](50) NOT NULL,
	[Username] [varchar](150) NOT NULL,
	[MobileNumber] [varchar](15) NOT NULL,
	[LicenseFor] [varchar](50) NOT NULL,
	[Issue] [varchar](20) NOT NULL,
	[Validity] [varchar](20) NOT NULL,
	[IssuedBy] [varchar](150) NOT NULL,
	[ApplyId] [int] NOT NULL,
 CONSTRAINT [PK_DrivingLicense] PRIMARY KEY CLUSTERED 
(
	[DrivingLicenseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[MedicalInfo]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MedicalInfo](
	[MedicalInfoId] [int] IDENTITY(1,1) NOT NULL,
	[ApplyId] [int] NOT NULL,
	[QAns1] [varchar](10) NOT NULL,
	[QAns2] [varchar](10) NOT NULL,
	[QAns3] [varchar](10) NOT NULL,
	[QAns4] [varchar](10) NOT NULL,
	[QAns5] [varchar](10) NOT NULL,
	[DoctorUsername] [varchar](150) NOT NULL,
	[Status] [varchar](10) NOT NULL,
	[CheckupDate] [varchar](20) NOT NULL,
 CONSTRAINT [PK_MedicalInfo] PRIMARY KEY CLUSTERED 
(
	[MedicalInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Offense]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Offense](
	[OffenseId] [int] IDENTITY(1,1) NOT NULL,
	[LicenseId] [varchar](50) NOT NULL,
	[OffenseType] [varchar](50) NOT NULL,
	[IssuedBy] [varchar](150) NOT NULL,
	[IssueDate] [varchar](20) NOT NULL,
	[Status] [varchar](20) NOT NULL,
 CONSTRAINT [PK_Offense] PRIMARY KEY CLUSTERED 
(
	[OffenseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PersonIdentity]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonIdentity](
	[IdentityId] [varchar](50) NOT NULL,
	[Name] [varchar](100) NOT NULL,
	[Father] [varchar](100) NOT NULL,
	[Mother] [varchar](100) NOT NULL,
	[DateOfBirth] [varchar](50) NOT NULL,
	[Sex] [varchar](50) NOT NULL,
	[MobileNumber] [varchar](50) NOT NULL,
	[PermanentAddress] [varchar](200) NOT NULL,
	[PlaceOfBirth] [varchar](50) NOT NULL,
	[BloodGroup] [varchar](50) NOT NULL,
	[Type] [varchar](50) NOT NULL,
	[Nationality] [varchar](50) NOT NULL,
	[Image] [varbinary](max) NULL,
 CONSTRAINT [PK_Identity] PRIMARY KEY CLUSTERED 
(
	[IdentityId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TestInfo]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TestInfo](
	[TestInfoId] [int] IDENTITY(1,1) NOT NULL,
	[ApplyId] [int] NOT NULL,
	[TestType] [varchar](20) NOT NULL,
	[QAns1] [varchar](10) NOT NULL,
	[QAns2] [varchar](10) NOT NULL,
	[QAns3] [varchar](10) NOT NULL,
	[QAns4] [varchar](10) NOT NULL,
	[QAns5] [varchar](10) NOT NULL,
	[TesterUsername] [varchar](150) NOT NULL,
	[Status] [varchar](20) NOT NULL,
	[TestDate] [varchar](20) NOT NULL,
 CONSTRAINT [PK_TestInfo] PRIMARY KEY CLUSTERED 
(
	[TestInfoId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Users]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Users](
	[Username] [varchar](150) NOT NULL,
	[Password] [varchar](50) NOT NULL,
	[IdentityId] [varchar](50) NOT NULL,
	[Status] [varchar](50) NOT NULL,
 CONSTRAINT [PK_Users] PRIMARY KEY CLUSTERED 
(
	[Username] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[VehicleLicense]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[VehicleLicense](
	[VehicleLicenseId] [varchar](50) NOT NULL,
	[Username] [varchar](150) NOT NULL,
	[MobileNumber] [varchar](15) NOT NULL,
	[Vehicle] [varchar](50) NOT NULL,
	[Vin] [varchar](25) NOT NULL,
	[VehicleType] [varchar](50) NOT NULL,
	[Issue] [varchar](20) NOT NULL,
	[Validity] [varchar](20) NOT NULL,
	[IssuedBy] [varchar](150) NOT NULL,
	[ApplyId] [int] NOT NULL,
 CONSTRAINT [PK_VehicleLicense] PRIMARY KEY CLUSTERED 
(
	[VehicleLicenseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
ALTER TABLE [dbo].[Apply] ADD  CONSTRAINT [DF_Apply_TestDate]  DEFAULT ('Pending') FOR [TestDate]
GO
ALTER TABLE [dbo].[Apply] ADD  CONSTRAINT [DF_Apply_IsIssued]  DEFAULT ('Pending') FOR [IsIssued]
GO
ALTER TABLE [dbo].[Offense] ADD  CONSTRAINT [DF_Offense_Status]  DEFAULT ('Pending') FOR [Status]
GO
/****** Object:  StoredProcedure [dbo].[DrivingLicensesNotIssuedYet]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[DrivingLicensesNotIssuedYet]
AS
SELECT Apply.ApplyId,Apply.Username,Apply.LicenseType,Apply.ApplyFor,TestInfo.Status
FROM Apply
RIGHT JOIN TestInfo ON Apply.ApplyId=TestInfo.ApplyId
WHERE Apply.IsIssued='Pending' AND Apply.LicenseType='Driving License'
ORDER BY Apply.ApplyId
GO
/****** Object:  StoredProcedure [dbo].[VehicleLicensesNotIssuedYet]    Script Date: 10/19/2020 11:54:28 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[VehicleLicensesNotIssuedYet]
AS
SELECT Apply.ApplyId,Apply.Username,Apply.LicenseType,Apply.ApplyFor,TestInfo.Status
FROM Apply
RIGHT JOIN TestInfo ON Apply.ApplyId=TestInfo.ApplyId
WHERE Apply.IsIssued='Pending' AND Apply.LicenseType='Vehicles License'
ORDER BY Apply.ApplyId
GO
USE [master]
GO
ALTER DATABASE [SmartLicenseDB] SET  READ_WRITE 
GO
