﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartLicenseWebApp.DAL.Gateway;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.BLL
{
    public class Manager
    {
        Gateway aGateway = new Gateway();
        PersonIdentity identity = new PersonIdentity();
        Users aUsers = new Users();
        Apply aApply = new Apply();
        List<Apply> applyList = new List<Apply>();
        List<DrivingLicense> drivingLicenseList = new List<DrivingLicense>();
        List<VehicleLicense> vehicleLicenseList = new List<VehicleLicense>();

        /*---------------------------------------------------------*/

        public string SaveUsers(Users aUsers)
        {
            if (aUsers.IdentityId == "" || aUsers.IdentityId.Length < 10)
            {
                return "Identity Box Fill Properly";
            }
            else if (aUsers.Username == "" || aUsers.Username.Length < 12)
            {
                return "Username TextBox Fill Properly";
            }
            else if (aUsers.Password == "" || aUsers.Password.Length < 5)
            {
                return "Password TextBox Fill Properly";
            }
            else
            {
                if (aGateway.PersonExist(aUsers.IdentityId))
                {
                    identity = aGateway.GetPersonIdentity(aUsers.IdentityId);
                    int age = identity.CalculateAge(Convert.ToDateTime(identity.DateOfBirth));
                    if (age >= 18)
                    {
                        if (aGateway.UserExistByIndentityId(aUsers))
                        {
                            return "User Identity Already Exists ! ! !";
                        }
                        else
                        {
                            if (aGateway.UserExistByUsername(aUsers.Username))
                            {
                                return "This Email :" + aUsers.Username + " Already Exist!!!";
                            }
                            else
                            {
                                int rowAffect = aGateway.SaveUsers(aUsers);
                                if (rowAffect > 0)
                                {
                                    return "Save";
                                }
                                else
                                {
                                    return "Operation Fail";
                                }
                            }

                        }
                    }
                    else
                    {
                        return "You Are Under 18 ! ! !";
                    }
                }
                else
                {
                    return "Person Not Found";
                }
            }
        }


        public Users GetUsers(string username)
        {
            if (aGateway.UserExistByUsername(username))
            {
                return aGateway.GetUserByUsername(username);
            }
            else
            {
                return null;
            }
        }


        public List<Apply> GetAllApplyDetails(string username)
        {
            applyList = aGateway.GetAllApplyDetails(username);
            return applyList;
        }


        public string SaveApplyInfo(Apply aApply, Bkash aBkash)
        {
            string message = Bkash.PaymentStatus(aBkash);

            if (message == "Payment Successful")
            {
                int rowAffect = aGateway.SaveApplyInfo(aApply);
                if (rowAffect > 0)
                {
                    return "Save";
                }
                else
                {
                    return "Operation Fail";
                }
            }
            else
            {
                return message;
            }

        }


        public Bkash GetBkashInfo(string bkashNumber)
        {
            if (aGateway.BkashNumberExist(bkashNumber))
            {
                return aGateway.GetBkashInfo(bkashNumber);
            }
            else
            {
                return null;
            }
        }


        public string UpdateBkashAmount(string bkashNumber, int newAmmount)
        {
            int rowAffect = aGateway.UpdateBkashAmount(bkashNumber, newAmmount);

            if (rowAffect > 0)
            {
                return "Save Successful";
            }
            else
            {
                return "Operation Fail";
            }
        }

        public string SearchPatientInApply(string patientUsername, int applyId)
        {
            if (aGateway.SearchPatientInApply(patientUsername, applyId))
            {
                return "Found";
            }
            else
            {
                return "Patient Not Apply For Driving License";
            }
        }

        public PersonIdentity GetPersonDetails(string patient)
        {
            string identityId = aGateway.GetUserByUsername(patient).IdentityId;
            identity = aGateway.GetPersonIdentity(identityId);
            return identity;
        }

        public string SaveMedicalInfo(MedicalInfo aInfo)
        {
            string s = "Select From List";
            if (aInfo.QAns1 == s)
            {
                return "Is there any defect of Vision?";
            }
            else if (aInfo.QAns2 == s)
            {
                return "Does the applicant suffer from Night Blindness?";
            }
            else if (aInfo.QAns3 == s)
            {
                return "Is there any defect of Hearing?";
            }
            else if (aInfo.QAns4 == s)
            {
                return "Does he show any evidence of being addicted to the excessive use of Alcohol/Drugs?";
            }
            else if (aInfo.QAns5 == s)
            {
                return "Is he fit for driving license?";
            }
            else if (aInfo.Status == s)
            {
                return "Please Certify the Patient";
            }
            else
            {
                if (aGateway.MedicalInfoExist(aInfo.ApplyId))
                {
                    return "Medical CheckUp for ApplyId "+aInfo.ApplyId+" already exist";
                }
                else
                {
                    int rowAffect = aGateway.SaveMedicalInfo(aInfo);
                    if (rowAffect > 0)
                    {
                        return "Save";
                    }
                    else
                    {
                        return "Operation Fail";
                    }
                }
            }
        }

        public MedicalInfo GetMedicalInfoByApplyId(int applyId)
        {
            if (aGateway.MedicalInfoExist(applyId))
            {
                return aGateway.GetMedicalInfoByApplyId(applyId);
            }
            else
            {
                return null;
            }
        }

        public Apply GetParticipantInApply(string participantUsername, int applyId)
        {
            if (aGateway.ParticipantExistInApply(participantUsername, applyId))
            {
                return aGateway.GetParticipantInApply(participantUsername, applyId);
            }
            else
            {
                return null;
            }
        }

        public string SaveDrivingTestInfo(TestInfo aTestInfo)
        {
            string s = "Select From List";
            if (aTestInfo.QAns1 == s)
            {
                return "Select Driving Test Exam Result";
            }
            else if (aTestInfo.QAns2 == s)
            {
                return "Select Vehicle Turning Result";
            }
            else if (aTestInfo.QAns3 == s)
            {
                return "Select Vehicle Back Driving Result";
            }
            else if (aTestInfo.QAns4 == s)
            {
                return "Select Vehicle Parking Result";
            }
            else if (aTestInfo.QAns5 == s)
            {
                return "Is he fit for driving license?";
            }
            else if (aTestInfo.Status == s)
            {
                return "Please Certify the Participant";
            }
            else
            {

                if (aGateway.TestInfoExistByApplyId(aTestInfo.ApplyId))
                {
                    return "Test Info Already Exist!!!";
                }
                else
                {
                    int rowAffect = aGateway.SaveTestInfo(aTestInfo);
                    if (rowAffect > 0)
                    {
                        return "Save";
                    }
                    else
                    {
                        return "Operation Fail";
                    }
                }
            }
        }

        public List<Apply> GetAllApply()
        {
            applyList = aGateway.GetAllApply();
            return applyList;
        }

        public Apply GetParticipantDetailsInApply(int applyId)
        {
            return aGateway.GetParticipantDetailsInApply(applyId);
        }

        public string GiveDateInApply(string date, int applyId)
        {
            string currentDate = DateTime.Now.ToString("MM/dd/yyyy");
            if (date == "")
            {
                return "Enter a Valid Date!!!!!!";
            }
            else if (DateTime.Parse(date) < DateTime.Parse(currentDate))
            {
                return "You Enter an Invalid Date!!!!!!";
            }
            else
            {
                int rowAffect = aGateway.GiveDateInApply(date, applyId);
                if (rowAffect > 0)
                {
                    return "Save";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }

        public TestInfo GetTestInfoByApplyId(int applyId)
        {
            if (aGateway.TestInfoExistByApplyId(applyId))
            {
                return aGateway.GetTestInfoByApplyId(applyId);
            }
            else
            {
                return null;
            }
        }

        public string SaveVehicleTestInfo(TestInfo aTestInfo)
        {
            string s = "Select From List";
            if (aTestInfo.QAns1 == s)
            {
                return "Verify Vehicle Document!!!!!!!";
            }
            else if (aTestInfo.QAns2 == "" || aTestInfo.QAns2.Length < 10 || aTestInfo.QAns2.Length > 10)
            {
                return "Vehicle Identification Number Enter Properly!!!!!!!";
            }
            else if (aTestInfo.QAns3 == "" || aTestInfo.QAns3.Length < 4)
            {
                return "Please Type Appropriate Company Name!!!!!";
            }
            else if (aTestInfo.QAns4 == "" || aTestInfo.QAns4.Length < 4 || aTestInfo.QAns4.Length > 4)
            {
                return "Please Type Appropriate Model Year!!!!!";
            }
            else if (aTestInfo.QAns5 == s)
            {
                return "Please Select Vehicle Class Letters!!!!!!!";
            }
            else if (aTestInfo.Status == s)
            {
                return "Please Certify the Vehicle!!!";
            }
            else
            {

                if (aGateway.TestInfoExistByApplyId(aTestInfo.ApplyId))
                {
                    return "Test Info Already Exist!!!";
                }
                else if (aGateway.VinInfoExistByVinId(aTestInfo.QAns2))
                {
                    return "VIN is already Registered!!!!!";
                }
                else
                {
                    int rowAffect = aGateway.SaveTestInfo(aTestInfo);
                    if (rowAffect > 0)
                    {
                        return "Save";
                    }
                    else
                    {
                        return "Operation Fail";
                    }
                }
            }
        }

        public List<Apply> GetAllTestedApply(string licenseType)
        {
            applyList = aGateway.GetAllTestedApply(licenseType);
            return applyList;
        }

        public string UpdateApplyIssue(int applyId, string isIssue)
        {
            int rowAffect = aGateway.UpdateApplyIssue(applyId, isIssue);

            if (rowAffect > 0)
            {
                return "Update";
            }
            else
            {
                return "Operation Fail";
            }
        }

        public string SaveDrivingLicense(DrivingLicense aDrivingLicense)
        {
            if (aDrivingLicense.DrivingLicenseId == "" || aDrivingLicense.DrivingLicenseId.Length < 10 ||
                aDrivingLicense.DrivingLicenseId.Length > 15)
            {
                return "Type a Valid Registration Number!!!!!!";
            }
            else
            {

                if (aGateway.DrivingLicenseExist(aDrivingLicense.DrivingLicenseId))
                {
                    return "Registration Number Already Exist!!!!!";
                }
                else
                {
                    int rowAffect = aGateway.SaveDrivingLicense(aDrivingLicense);
                    if (rowAffect > 0)
                    {
                        string message = UpdateApplyIssue(aDrivingLicense.ApplyId, "Issued");
                        return "Save";
                    }
                    else
                    {
                        return "Operation Fail";
                    }
                }
            }
        }

        public string SaveVehicleLicense(VehicleLicense aVehicleLicense)
        {
            if (aGateway.VehicleLicenseExist(aVehicleLicense.VehicleLicenseId))
            {
                return "Registration Number Already Exist!!!!!";
            }
            else
            {
                int rowAffect = aGateway.SaveVehicleLicense(aVehicleLicense);
                if (rowAffect > 0)
                {
                    string message = UpdateApplyIssue(aVehicleLicense.ApplyId, "Issued");
                    return "Save";
                }
                else
                {
                    return "Operation Fail";
                }
            }
        }

        public List<DrivingLicense> GetDrivingLicense(string searchType, string searchId)
        {
            if (searchType=="LicenseId")
            {
                drivingLicenseList = aGateway.GetDrivingLicense("DrivingLicenseId",searchId);
                return drivingLicenseList;
            }
            else
            {
                drivingLicenseList = aGateway.GetDrivingLicense(searchType,searchId);
                return drivingLicenseList;
            }
            
        }

        public List<VehicleLicense> GetVehicleLicense(string searchType, string searchId)
        {
            if (searchType == "LicenseId")
            {
                vehicleLicenseList = aGateway.GetVehicleLicense("VehicleLicenseId", searchId);
                return vehicleLicenseList;
            }
            else
            {
                vehicleLicenseList = aGateway.GetVehicleLicense(searchType, searchId);
                return vehicleLicenseList;
            }
        }

        public DrivingLicense GetDrivingLicenseByLicenseNumber(string licenseId)
        {
            return aGateway.GetDrivingLicenseByLicenseNumber(licenseId);
        }

        public string SaveOffense(Offense aOffense)
        {
            int rowAffect = aGateway.SaveOffense(aOffense);
            if (rowAffect > 0)
            {
                return "Save";
            }
            else
            {
                return "Operation Fail";
            }
        }

        public List<Offense> GetLicenseOffenses(string licenseId)
        {
            return aGateway.GetLicenseOffenses(licenseId);
        }

        public VehicleLicense GetVehicleLicenseByLicenseNumber(string licenseId)
        {
            return aGateway.GetVehicleLicenseByLicenseNumber(licenseId);
        }
    }
}