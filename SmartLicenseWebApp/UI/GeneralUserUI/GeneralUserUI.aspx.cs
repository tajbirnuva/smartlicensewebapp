﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;

namespace SmartLicenseWebApp.UI.GeneralUserUI
{
    public partial class GeneralUserUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        protected void Page_Load(object sender, EventArgs e)
        {
                if (Session["User"] != null)
                {
                    string username = Session["User"].ToString();

                    //Driving License
                    drivingLicenseGridView.DataSource = aManager.GetDrivingLicense("Username", username);
                    drivingLicenseGridView.DataBind();

                    //Vehicle License
                    vehicleLicenseGridView.DataSource = aManager.GetVehicleLicense("Username", username);
                    vehicleLicenseGridView.DataBind();
                }
                else
                {
                    Response.Redirect("../WelcomeUI.aspx");
                }

        }
        protected void ShowDrivingLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            string licenseId = idHiddenField.Value;
            Response.Redirect("DrivingLicenseUI.aspx?licenseId=" + licenseId);
        }



        protected void ShowVehicleLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            string licenseId = idHiddenField.Value;
            Response.Redirect("VehicleLicenseUI.aspx?licenseId=" + licenseId);
        }
    }
}