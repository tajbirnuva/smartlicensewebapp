﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralUserApplyUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.GeneralUserUI.GeneralUserApplyUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apply</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="GeneralUserUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="GeneralUserUI.aspx" class="smoothScroll">Home</a></li>
                <li><a href="GeneralUserApplyUI.aspx" class="smoothScroll">Apply</a></li>
                <li class="appointment-btn"><a href="../WelcomeUI.aspx" class="smoothScroll">Logout</a></li>
            </ul>
        </div>

    </div>
</section>

    
      <form id="form2" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Apply For License</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">

                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">License Type</label>
                                      <asp:DropDownList ID="LicenseTypeDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Driving License</asp:ListItem>
                                          <asp:ListItem>Vehicles License</asp:ListItem>
                                      </asp:DropDownList>
                                     
                                  </div>

                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Apply For</label>
                                      <asp:DropDownList ID="applyDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Two Wheels</asp:ListItem>
                                          <asp:ListItem>Three Wheels</asp:ListItem>
                                          <asp:ListItem>Four Wheels</asp:ListItem>
                                          <asp:ListItem>Both Two & Four</asp:ListItem>
                                      </asp:DropDownList>
                                     
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Utility Paper(Gas/Electricity Bill Scan Copy)</label>
                                      <asp:FileUpload ID="utilityImageFileUpload" class="form-control" runat="server" />
                                     
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Academic Transcript/Vehicles Document(Scan Copy)</label>
                                      <label for="date" style="background-color: #CCFF66">--For Driving License Academic Transcript Or For Vehicles License Vehicles Document--</label>
                                      <asp:FileUpload ID="documentImageFileUpload" class="form-control" runat="server" />
                                     
                                  </div>

                                  <div class="col-md-9 col-sm-9">
                                     
                                      <asp:Button ID="applyButton" class="form-control" runat="server" Text="Apply" OnClick="applyButton_Click"  />
                                     
                                  </div>

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              
              <div class="wow fadeInUp" data-wow-delay="0.8s">
                
                  <div class="col-md-12 col-sm-12">
                      <h4>Apply Details</h4>
                      <asp:GridView ID="applyReportGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />


                          <Columns>
                
                              <asp:TemplateField HeaderText="License Type">
                                  <ItemTemplate>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("ApplyId")%>'/>
                                      <asp:Label runat="server" Text='<%#Eval("LicenseType")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              

                              <asp:TemplateField HeaderText="Apply For">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("ApplyFor")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="TestDate">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("TestDate")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>


                              <asp:TemplateField HeaderText="Status">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("IsIssued")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Details">
                                  <ItemTemplate>
                                      <asp:LinkButton ID="showLinkButton" runat="server" OnClick="ShowLinkButton_OnClick">Show</asp:LinkButton>
                                    </ItemTemplate>
                              </asp:TemplateField>
                
                             

                          </Columns>
                          

                      </asp:GridView>
                  </div>
                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>