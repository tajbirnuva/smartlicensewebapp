﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="GeneralUserPaymentUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.GeneralUserUI.GeneralUserPaymentUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apply</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="GeneralUserUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                  <li class="appointment-btn"><a href="GeneralUserApplyUI.aspx">Back to Apply</a></li>
            </ul>
        </div>

    </div>
</section>

    
      <form id="form2" runat="server">
          <section id="payment">
              <div class="container">
                  <div class="row">
                   
                      <div class="col-md-9 col-sm-9">
                          <div class="clearfix"></div>
                          <div class="col-md-6 col-sm-6">
                              <div class="about-info">
                                  <div class="wow fadeInUp" data-wow-delay="0.8s">
                                      
                                      <div class="col-md-12 col-sm-12">
                                          <label for="email">Bkash Account Number</label>
                                          <asp:TextBox ID="numberTextbox" class="form-control" placeholder="Enter Your Bkash Account Number" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="col-md-12 col-sm-12">
                                          <label for="email">Amount</label>
                                          <asp:TextBox ID="amountTextBox" class="form-control" placeholder="Enter Your Amount" runat="server"></asp:TextBox>
                                      </div>
                                      <div class="col-md-12 col-sm-12">
                                          <label for="telephone">Pin</label>
                                          <asp:TextBox ID="pinTextBox" class="form-control" placeholder="Enter Your Bkash Pin" TextMode="Password"  runat="server"></asp:TextBox>
                                      </div>

                                      <div class="col-md-12 col-sm-12">
                                          <label for="telephone"></label>
                                          <asp:Button ID="PayButton" class="form-control" runat="server" Text="Make Payment" OnClick="PayButton_Click"  />
                                     
                                      </div>

                                  </div>
                              </div>
                          </div>
                      </div>
                      <div class="col-md-3 col-sm-3">
                          <div class="about-info">
                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  <i class='fas fa-motorcycle' style='font-size:25px'></i>
                                  <p>Two Wheels 4000</p>
                                  <i class='fas fa-truck' style='font-size:25px'></i>
                                  <p>Four Wheels 8000</p>
                                  <i class='fas fa-motorcycle' style='font-size:25px'></i>
                                  <i class='fas fa-truck' style='font-size:25px'></i>
                                  <p>For Both Wheels 10000</p>
                              </div>
                          </div>
                      </div>
                    
                  </div>
              </div>
          </section>

      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>