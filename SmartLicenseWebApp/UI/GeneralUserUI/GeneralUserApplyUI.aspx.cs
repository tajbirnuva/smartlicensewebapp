﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.GeneralUserUI
{
    public partial class GeneralUserApplyUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string username = Session["User"].ToString();

                //Load All Apply
                applyReportGridView.DataSource = aManager.GetAllApplyDetails(username);
                applyReportGridView.DataBind();
            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void applyButton_Click(object sender, EventArgs e)
        {
            Apply aApply = new Apply();
            aApply.Username = Session["User"].ToString();
            aApply.LicenseType = LicenseTypeDropDownList.SelectedValue;
            aApply.ApplyFor = applyDropDownList.SelectedValue;
            if (aApply.Username != "" && aApply.LicenseType != "Select From List" && aApply.ApplyFor != "Select From List")
            {
                if (aApply.LicenseType== "Vehicles License" && aApply.ApplyFor== "Both Two & Four")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('For Vehicles License You Can not Select Both Two & Four');", true);
                }
                else
                {
                    HttpPostedFile postedFile = utilityImageFileUpload.PostedFile;
                    string fileName = Path.GetFileName(postedFile.FileName);
                    string fileExtention = Path.GetExtension(fileName);
                    if (fileExtention.ToLower() == ".jpg" || fileExtention.ToLower() == ".png" || fileExtention.ToLower() == ".jpeg")
                    {
                        Stream stream = postedFile.InputStream;
                        BinaryReader binaryReader = new BinaryReader(stream);
                        aApply.UtilityImage = binaryReader.ReadBytes((int)stream.Length);

                        HttpPostedFile postedFile2 = documentImageFileUpload.PostedFile;
                        string fileName2 = Path.GetFileName(postedFile2.FileName);
                        string fileExtention2 = Path.GetExtension(fileName2);
                        if (fileExtention2.ToLower() == ".jpg" || fileExtention2.ToLower() == ".png" || fileExtention2.ToLower() == ".jpeg")
                        {
                            Stream stream2 = postedFile2.InputStream;
                            BinaryReader binaryReader2 = new BinaryReader(stream2);
                            aApply.DocumentImage = binaryReader2.ReadBytes((int)stream2.Length);


                            Session["Apply"] = aApply;
                            Response.Redirect("GeneralUserPaymentUI.aspx");

                        }
                        else
                        {
                            ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Select Proper Image File (.jpg/.png) For Academic Transcript/Vehicles Document');", true);
                        }


                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Select Proper Image File (.jpg/.png) For Utility Document');", true);
                    }

                   
                }
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Please Select Properly!!!!!!!!');", true);
            }
        }


        protected void ShowLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
             DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
             GridViewRow row = (GridViewRow)cell.Parent;
             HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
             int applyId = Convert.ToInt32(idHiddenField.Value);
             Response.Redirect("ApplyLicenseDetailsUI.aspx?applyId="+ applyId);
        }


    }
}

