﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.GeneralUserUI
{
    public partial class GeneralUserPaymentUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void PayButton_Click(object sender, EventArgs e)
        {
            Apply aApply=new Apply();
            aApply = (Apply) Session["Apply"];

            if (numberTextbox.Text =="" || numberTextbox.Text.Length<11 || numberTextbox.Text.Length > 11)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Your Bkash Number Properly!!!!!!!!');", true);
            }
            else if (amountTextBox.Text =="" || System.Text.RegularExpressions.Regex.IsMatch(amountTextBox.Text, "[^0-9]"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter an Valid Amount!!!!!!!');", true);
            }
            else if (pinTextBox.Text == "" || pinTextBox.Text.Length<4)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter Your Pin Number!!!!!!!!');", true);
            }
            else
            { 
                Bkash aBkash = new Bkash();
                aBkash.Number = numberTextbox.Text;
                aBkash.Amount = Convert.ToInt32(amountTextBox.Text);
                aBkash.Pin = pinTextBox.Text;
                string msg = Apply.AmountForApplied(aApply.ApplyFor, aBkash.Amount);
                if (msg!= "Success")
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + msg + "');", true);
                }
                else
                {
                    string message = aManager.SaveApplyInfo(aApply, aBkash);
                    if (message == "Save")
                    {
                        Response.Redirect("GeneralUserApplyUI.aspx");
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
                    }
                }


            }
        }
    }
}