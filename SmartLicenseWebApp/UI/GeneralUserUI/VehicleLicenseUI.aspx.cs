﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.GeneralUserUI
{
    public partial class VehicleLicenseUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        string licenseId;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Session["User"] != null)
            {
                licenseId = Request.QueryString["licenseId"];
                VehicleLicense aLicense = aManager.GetVehicleLicenseByLicenseNumber(licenseId);

                //Participant details
                PersonIdentity person = aManager.GetPersonDetails(aLicense.Username);
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = person.Father;
                mobileLabel.Text = person.MobileNumber;
                birthdateLabel.Text = "Date of Birth: " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group: " + person.BloodGroup;

                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);

                // License Details
                licenseIdLabel.Text = aLicense.VehicleLicenseId;
                vinLabel.Text = "VIN :  " + aLicense.Vin;
                vehicleLabel.Text = "Vehicle :  " + aLicense.Vehicle;

                issueLabel.Text = "Issue :  " + aLicense.Issue;
                validityLabel.Text = "Validity :  " + aLicense.Validity;
                issueByLabel.Text = "Issued By :  " + aLicense.IssuedBy;


                //Offense Details
                offenseGridView.DataSource = aManager.GetLicenseOffenses(licenseId);
                offenseGridView.DataBind();
                //License Status

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }
    }
}