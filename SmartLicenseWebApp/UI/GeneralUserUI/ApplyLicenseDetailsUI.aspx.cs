﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.GeneralUserUI
{
    public partial class ApplyLicenseDetailsUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        MedicalInfo aMedicalInfo=new MedicalInfo();
        TestInfo aTestInfo=new TestInfo();
        protected void Page_Load(object sender, EventArgs e)
        {
            int applyId = Convert.ToInt32(Request.QueryString["applyId"]);
            applyIdLabel.Text = "Apply ID: " + applyId;

            aMedicalInfo = aManager.GetMedicalInfoByApplyId(applyId);
            if (aMedicalInfo!=null)
            {
                checkupStatusLabel.Text = "Checkup Complete";
                dateLabel.Text = "checkup Date: " + aMedicalInfo.CheckupDate;
                if (aMedicalInfo.Status== "Approve")
                {
                    approveLabel.BackColor=Color.Green;
                    approveLabel.ForeColor=Color.White;
                    approveLabel.Text = "You are " + aMedicalInfo.Status + "d For Driving License";
                }
                else
                {
                    approveLabel.BackColor = Color.Red;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = "You are " + aMedicalInfo.Status + " For Driving License";
                }
                
            }
            else
            {
                checkupStatusLabel.Text = "Not Checkup Yet";
            }

            aTestInfo = aManager.GetTestInfoByApplyId(applyId);
            if (aTestInfo != null)
            {
                testStatusLabel.Text = "Test Complete";
                dateTestLabel.Text = "Test Date: " + aTestInfo.TestDate;
                if (aTestInfo.Status == "Approve")
                {
                    approveTestLabel.BackColor = Color.Green;
                    approveTestLabel.ForeColor = Color.White;
                    approveTestLabel.Text = "You are " + aTestInfo.Status + "d For License";
                }
                else
                {
                    approveLabel.BackColor = Color.Red;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = "You are " + aTestInfo.Status + " For License";
                }

            }
            else
            {
                testStatusLabel.Text = "Not Test Yet";
            }



        }
    }
}