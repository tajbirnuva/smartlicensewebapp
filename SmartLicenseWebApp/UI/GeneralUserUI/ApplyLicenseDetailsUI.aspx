﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ApplyLicenseDetailsUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.GeneralUserUI.ApplyLicenseDetailsUI" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Apply</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="GeneralUserUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="GeneralUserUI.aspx" class="smoothScroll">Home</a></li>
                <li><a href="GeneralUserApplyUI.aspx" class="smoothScroll">Apply</a></li>

                <li class="appointment-btn"><a href="../WelcomeUI.aspx" class="smoothScroll">Logout</a></li>
            </ul>
        </div>

    </div>
</section>

    
      <form id="form2" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">
            <h3><asp:Label ID="applyIdLabel" runat="server" Text=""></asp:Label></h3>
            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">

                                  <div class="col-md-9 col-sm-9">
                                    <h4><label for="date">Medical Checkup Info</label></h4>  
                                      
                                  </div>

                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="checkupStatusLabel" runat="server" Text="Checkup Done"></asp:Label>
                                  </div>
                                 
                                  

                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="dateLabel" runat="server" Text=""></asp:Label>
                                      
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="approveLabel" runat="server" Text=""></asp:Label>
                                  </div>

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   
              </div>
              
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                  <div class="wow fadeInUp" data-wow-delay="0.8s">

                      <div class="col-md-9 col-sm-9">
                          <h4><label for="date">BRTA Test Info</label></h4>  
                                      
                      </div>

                      <div class="col-md-9 col-sm-9">
                          <asp:Label ID="testStatusLabel" runat="server" Text=""></asp:Label>
                      </div>
                                 
                                  

                      <div class="col-md-9 col-sm-9">
                          <asp:Label ID="dateTestLabel" runat="server" Text=""></asp:Label>
                                      
                      </div>
                      <div class="col-md-9 col-sm-9">
                          <asp:Label ID="approveTestLabel" runat="server" Text=""></asp:Label>
                      </div>

                  </div>
                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>
