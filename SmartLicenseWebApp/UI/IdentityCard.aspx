﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="IdentityCard.aspx.cs" Inherits="SmartLicenseWebApp.UI.IdentityCard" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Medical Reports</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href="/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/animate.css">
    <link rel="stylesheet" href="/css/owl.carousel.css">
    <link rel="stylesheet" href="/css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="/css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
           
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="WelcomeUI.aspx" class="smoothScroll">Home</a></li>
              
            </ul>
        </div>
    </div>
</section>




    
      <form id="form1" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>NID/Passport Details</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  
                                  <div class="col-md-6 col-sm-6">
                                      <label for="name">Identity No</label>
                                      <asp:TextBox ID="idTextbox" class="form-control" placeholder="Number length > 9" runat="server"></asp:TextBox>

                                  </div>

                                  <div class="col-md-6 col-sm-6">
                                      <label for="date">Type</label>
                                      <asp:DropDownList ID="typeDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select a Type</asp:ListItem>
                                          <asp:ListItem>NID</asp:ListItem>
                                          <asp:ListItem>Passport</asp:ListItem>
                                          
                                      </asp:DropDownList>
                                     
                                  </div>
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Name</label>
                                      <asp:TextBox ID="NameTextBox" class="form-control" placeholder="Enter Your name" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Gender</label>
                                      <asp:TextBox ID="genderTextBox1" class="form-control" placeholder="Enter Your Gender" runat="server"></asp:TextBox>
                                  </div>
                                  

                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Fathe </label>
                                      <asp:TextBox ID="fatherTextBox" class="form-control" placeholder="Enter Your Father Name" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Mother</label>
                                      <asp:TextBox ID="motherTextBox" class="form-control" placeholder="Enter Your Mother Name" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Birthday</label>
                                      <asp:TextBox ID="birthdayTextBox" class="form-control" placeholder="Enter Your Birthday" runat="server"></asp:TextBox>
                                  </div>
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">P Address</label>
                                      <asp:TextBox ID="addressTextBox1" class="form-control" placeholder="Enter Your parmanent Address" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Birth Place</label>
                                      <asp:TextBox ID="birthplaceTextBox2" class="form-control" placeholder="Enter Your Birth Place" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Blood Group</label>
                                      <asp:TextBox ID="bloodTextBox1" class="form-control" placeholder="Enter Your Blood Group" runat="server"></asp:TextBox>
                                  </div>
                                  
                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Mobile</label>
                                      <asp:TextBox ID="mobileTextBox1" class="form-control" placeholder="Enter Your Mobile" runat="server"></asp:TextBox>
                                  </div>
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">Nationality</label>
                                      <asp:TextBox ID="nationaliTextBox1" class="form-control" placeholder="Enter Your Nationality" runat="server"></asp:TextBox>
                                  </div>

                                  
                                  <div class="col-md-12 col-sm-12">
                                      <label for="name">File</label>
                                      <asp:FileUpload ID="imageFileUpload" class="form-control" runat="server" />
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                     
                                      <asp:Button ID="saveButton" class="form-control" runat="server" Text="Save" OnClick="saveButton_Click" />
                                     
                                  </div>

                                  <div class="col-md-12 col-sm-12">
                                      <asp:Label ID="saveLavel" runat="server" Text=""></asp:Label>
                                  </div>
                                  

                              </div>

        </div>
    
               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="/js/jquery.js"></script>
<script src="/js/bootstrap.min.js"></script>
<script src="/js/jquery.sticky.js"></script>
<script src="/js/jquery.stellar.min.js"></script>
<script src="/js/wow.min.js"></script>
<script src="/js/smoothscroll.js"></script>
<script src="/js/owl.carousel.min.js"></script>
<script src="/js/custom.js"></script>
</body>
</html>