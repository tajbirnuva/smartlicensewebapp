﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.DAL.Gateway;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI
{
    public partial class IdentityCard : System.Web.UI.Page
    {
        Gateway aGateway=new Gateway();
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            HttpPostedFile postedFile = imageFileUpload.PostedFile;
            string fileName = Path.GetFileName(postedFile.FileName);
            string fileExtention = Path.GetExtension(fileName);
            if (fileExtention.ToLower() == ".jpg" || fileExtention.ToLower() == ".png" || fileExtention.ToLower() == ".jpeg")
            {   PersonIdentity aIdentity=new PersonIdentity();

                aIdentity.IdentityId = idTextbox.Text;
                aIdentity.Name = NameTextBox.Text;
                aIdentity.Type = typeDropDownList.SelectedValue;
                aIdentity.Father = fatherTextBox.Text;
                aIdentity.Mother = motherTextBox.Text;
                aIdentity.MobileNumber = mobileTextBox1.Text;
                aIdentity.DateOfBirth = birthdayTextBox.Text;
                aIdentity.PlaceOfBirth = birthplaceTextBox2.Text;
                aIdentity.PermanentAddress = addressTextBox1.Text;
                aIdentity.Nationality = nationaliTextBox1.Text;
                aIdentity.BloodGroup = bloodTextBox1.Text;
                aIdentity.Sex = genderTextBox1.Text;
                
                Stream stream = postedFile.InputStream;
                BinaryReader binaryReader = new BinaryReader(stream);
                aIdentity.Image = binaryReader.ReadBytes((int)stream.Length);

                int raw = aGateway.SaveIdentity(aIdentity);

                if (raw >0 )
                {
                    saveLavel.Text = "Save Successful";
                    imageFileUpload.Attributes.Clear();
                   
                }
                else
                {
                    saveLavel.Text = "Not !!!!!!!!!!!";
                }

            }
            else
            {
                saveLavel.Text = "Select Proper Image File (.jpg/.png)";
                saveLavel.ForeColor = System.Drawing.Color.Red;
            }
        }
    }
}