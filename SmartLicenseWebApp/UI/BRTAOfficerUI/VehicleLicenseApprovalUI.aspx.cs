﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.BRTAOfficerUI
{
    public partial class VehicleLicenseApprovalUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        int applyId;
        string paticipant;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                applyId = Convert.ToInt32(Request.QueryString["applyId"]);
                paticipant = aManager.GetParticipantDetailsInApply(applyId).Username;

                //Apply Info
                Apply aApply = aManager.GetParticipantDetailsInApply(applyId);
                applyIdTextBox.Text = aApply.ApplyId.ToString();
                licenseForTextbox.Text = aApply.ApplyFor;
                issueTextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                validityTextBox.Text = DateTime.Now.AddYears(5).ToString("dd/MM/yyyy");

                string image1 = Convert.ToBase64String(aApply.UtilityImage);
                utilityImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image1);

                string image2 = Convert.ToBase64String(aApply.DocumentImage);
                documentImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image2);

                //Participant details
                PersonIdentity person = aManager.GetPersonDetails(paticipant);
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = person.Father;
                mobileLabel.Text = person.MobileNumber;
                birthdateLabel.Text = "Date of Birth: " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group: " + person.BloodGroup;

                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);


                //Test Info
                TestInfo aTestInfo = aManager.GetTestInfoByApplyId(applyId);
                testIdLabel.Text = "Test Info ID: " + aTestInfo.TestInfoId;
                testerLabel.Text = aManager.GetPersonDetails(aTestInfo.TesterUsername).Name;
                testDateLabel.Text = aTestInfo.TestDate;

                if (aTestInfo.Status == "Approve")
                {
                    testApprovalLabel.BackColor = Color.Green;
                    testApprovalLabel.ForeColor = Color.White;
                    testApprovalLabel.Text = aTestInfo.Status + "d For Driving License";
                }
                else
                {
                    testApprovalLabel.BackColor = Color.Red;
                    testApprovalLabel.ForeColor = Color.White;
                    testApprovalLabel.Text = aTestInfo.Status + " For License";
                }

                VehicleTextBox.Text = aTestInfo.QAns3 + " " + aTestInfo.QAns4;
                vinTextBox.Text = aTestInfo.QAns2;
                vehicleClassTextBox.Text = aTestInfo.QAns5;



            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void notIssueButton_Click(object sender, EventArgs e)
        {
            string message = aManager.UpdateApplyIssue(applyId, "Not Issue");
            if (message == "Update")
            {
                Response.Redirect("BrtaOfficerUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }

        protected void issueButton_Click(object sender, EventArgs e)
        {
            if (metroDropDown.SelectedValue == "Select From List")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Please select Reg. City!!!!!');",
                    true);

            }
            else if (regNumberTextBox.Text == "" || regNumberTextBox.Text.Length < 6 ||
                     regNumberTextBox.Text.Length > 6)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Please Type a 6-digit Valid Reg Number!!!!!');", true);

            }
            else
            {
                VehicleLicense aVehicleLicense = new VehicleLicense();
                aVehicleLicense.VehicleLicenseId = metroDropDown.SelectedValue + " " + vehicleClassTextBox.Text + " " +
                                                   regNumberTextBox.Text;
                aVehicleLicense.Username = paticipant;
                aVehicleLicense.MobileNumber = mobileLabel.Text;
                aVehicleLicense.Vehicle = VehicleTextBox.Text;
                aVehicleLicense.Vin = vinTextBox.Text;
                aVehicleLicense.VehicleType = licenseForTextbox.Text;
                aVehicleLicense.Issue = issueTextBox.Text;
                aVehicleLicense.Validity = validityTextBox.Text;
                aVehicleLicense.IssuedBy = Session["User"].ToString();
                aVehicleLicense.ApplyId = applyId;

                string message = aManager.SaveVehicleLicense(aVehicleLicense);
                if (message == "Save")
                {
                    Response.Redirect("BrtaOfficerUI.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
                }
            }
        }
    }
}