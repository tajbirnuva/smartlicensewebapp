﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleLicenseApprovalUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.BRTAOfficerUI.VehicleLicenseApprovalUI" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Driving License Approval</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">
    
    <style>
        .responsive {
            width: 100%;
            max-width: 300px;
            height: auto;
            border: 2px solid #555;
        }
        #utilityImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }
        #documentImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }

    </style>

</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>


<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="../BRTAOfficerUI/BrtaOfficerUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../BRTAOfficerUI/BrtaOfficerUI.aspx" class="smoothScroll">Home</a></li>
                <li><a href="#" class="smoothScroll"></a></li>
                <li><a href="#" class="smoothScroll">|</a></li>
                
                <li><a href="#team" class="smoothScroll">Participant</a></li>
                <li><a href="#telephone" class="smoothScroll">Documents</a></li>
                <li><a href="#news" class="smoothScroll">Registration</a></li>

            </ul>
        </div>

    </div>
</section>

<section id="team" data-stellar-background-ratio="1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <div>
                        <asp:Image ID="Image" runat="server" class="responsive" />
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div>
                        <h4 class="wow fadeInUp" data-wow-delay="0.1s"><asp:Label ID="nameLabel" runat="server" Text=""></asp:Label></h4>
                    </div>
                        
                    <div>
                        <h5> <asp:Label ID="identityLabel" runat="server" Text="" ></asp:Label></h5>
                    </div>
                    <div>
                        <asp:Label ID="fatherLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="mobileLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="birthdateLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="bloodLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                       

                </div>
                <div class="col-md-4 col-sm-4">

                    <div>
                        <h4 class="wow fadeInUp" data-wow-delay="0.1s"><asp:Label ID="testIdLabel" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div>
                        <h5> <asp:Label ID="testerLabel" runat="server" Text="" ></asp:Label></h5>
                    </div>
                    <div>
                        <asp:Label ID="testDateLabel" runat="server" Text=""></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="testApprovalLabel" runat="server" Text=""></asp:Label> 
                    </div>
                    <div> <label for="date">___________________</label></div>
                </div>

            </div>
        </div>
    </div>
</section>
 <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

               </div>
          </div>

     </section>













<section id="telephone" data-stellar-background-ratio="3">
    <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                    <h4>Utility Paper</h4>
                </div>

                <div class="wow fadeInUp" data-wow-delay="0.8s">

                    <div class="col-md-12 col-sm-12">
                        <asp:Image ID="utilityImage" runat="server" />
                    </div>


                </div>

            </div>
            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                    <h4>Transcript</h4>
                </div>

                <div class="wow fadeInUp" data-wow-delay="0.8s">

                    <div class="col-md-12 col-sm-12">
                        <asp:Image ID="documentImage" runat="server" />
                    </div>


                </div>

            </div>
        
        </div>
    </div>

</section>








      <form id="form2" runat="server">
       <section id="news" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

        <div class="col-md-12 col-sm-12">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h3>Registration Details</h3>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  <div class="col-md-6 col-sm-6">
                                      <div class="about-info">
                                          <div class="wow fadeInUp" data-wow-delay="0.8s">
                                      
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Apply ID For</label>
                                                  <asp:TextBox ID="applyIdTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Vehicle</label>
                                                  <asp:TextBox ID="VehicleTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                             
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Date of Issue</label>
                                                  <asp:TextBox ID="issueTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                           
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="telephone">License Registration City</label>
                                                  <asp:DropDownList ID="metroDropDown" class="form-control" runat="server">
                                                      <asp:ListItem>Select From List</asp:ListItem>
                                                      <asp:ListItem>Dhaka</asp:ListItem>
                                                      <asp:ListItem>Chittagong</asp:ListItem>
                                                      <asp:ListItem>Sylhet</asp:ListItem>
                                                  </asp:DropDownList> </div>
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Registration Number</label>
                                                  <asp:TextBox ID="regNumberTextBox" class="form-control" runat="server" placeholder="Enter Registration Number"></asp:TextBox>
                                              </div>

                                          </div>
                                      </div>
                                  </div>
                                  
                                  
                                  
                                  
                                  
                                     <div class="col-md-6 col-sm-6">
                                      <div class="about-info">
                                          <div class="wow fadeInUp" data-wow-delay="0.8s">
                                      
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">License For</label>
                                                  <asp:TextBox ID="licenseForTextbox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Vehicle Identification Numver</label>
                                                  <asp:TextBox ID="vinTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                            
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="telephone">Validity Date</label>
                                                  <asp:TextBox ID="validityTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="telephone">The letter Representing Vehicle Class</label>
                                                  <asp:TextBox ID="vehicleClassTextBox" class="form-control" runat="server" ReadOnly="True"></asp:TextBox>
                                              </div>
                                             
                                              <div class="col-md-5 col-sm-5">
                                                  <label for="telephone"></label>
                                                  <asp:Button ID="notIssueButton" class="form-control" runat="server" Text=" Not Issue" OnClick="notIssueButton_Click" />
                                     
                                              </div>
                                              <div class="col-md-7 col-sm-7">
                                                  <label for="telephone"></label>
                                                  <asp:Button ID="issueButton" class="form-control" runat="server" Text="Issue" OnClick="issueButton_Click" />
                                     
                                              </div>
                                             

                                          </div>
                                      </div>
                                  </div>
                                  

                              </div>

        </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>
