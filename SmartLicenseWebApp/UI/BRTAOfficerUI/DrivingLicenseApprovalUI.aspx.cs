﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.BRTAOfficerUI
{
    public partial class DrivingLicenseApprovalUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        int applyId;
        private string paticipant;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                applyId = Convert.ToInt32(Request.QueryString["applyId"]); 
                paticipant = aManager.GetParticipantDetailsInApply(applyId).Username;

                Apply aApply = aManager.GetParticipantDetailsInApply(applyId);
                string image1 = Convert.ToBase64String(aApply.UtilityImage);
                utilityImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image1);

                string image2 = Convert.ToBase64String(aApply.DocumentImage);
                documentImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image2);

                //Participant details
                PersonIdentity person = aManager.GetPersonDetails(paticipant);
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = person.Father;
                mobileLabel.Text = person.MobileNumber;
                birthdateLabel.Text = "Date of Birth: " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group: " + person.BloodGroup;

                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);

                // Medical Details
                MedicalInfo aMedicalInfo = aManager.GetMedicalInfoByApplyId(applyId);
                medicalIdLabel.Text = "Medical Info ID: " + aMedicalInfo.MedicalInfoId;
                visionLabel.Text = "- "+aMedicalInfo.QAns1;
                blindnessLabel.Text = "- " + aMedicalInfo.QAns2;
                hearingLabel.Text = "- " + aMedicalInfo.QAns3;
                addictedLabel.Text = "- " + aMedicalInfo.QAns4;
                doctorLabel.Text = aManager.GetPersonDetails(aMedicalInfo.DoctorUsername).Name;
                checkupDateLabel.Text = aMedicalInfo.CheckupDate;

                if (aMedicalInfo.Status == "Approve")
                {
                    approveLabel.BackColor = Color.Green;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = aMedicalInfo.Status + "d For Driving License";
                }
                else
                {
                    approveLabel.BackColor = Color.Red;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = aMedicalInfo.Status + " For Driving License";
                }

                //Test Info
                TestInfo aTestInfo = aManager.GetTestInfoByApplyId(applyId);
                testIdLabel.Text = "Test Info ID: " + aTestInfo.TestInfoId;
                testerLabel.Text = aManager.GetPersonDetails(aTestInfo.TesterUsername).Name;
                testDateLabel.Text = aTestInfo.TestDate;

                drivingTestResultLabel.Text = "- " + aTestInfo.QAns1;
                turningLabel.Text = "- " + aTestInfo.QAns2;
                backDrivingLabel.Text = "- " + aTestInfo.QAns3;
                parkingLabel.Text = "- " + aTestInfo.QAns4;
                certifyLabel.Text = "- " + aTestInfo.QAns5;

                if (aTestInfo.Status == "Approve")
                {
                    testApprovalLabel.BackColor = Color.Green;
                    testApprovalLabel.ForeColor = Color.White;
                    testApprovalLabel.Text = aTestInfo.Status + "d For Driving License";
                }
                else
                {
                    testApprovalLabel.BackColor = Color.Red;
                    testApprovalLabel.ForeColor = Color.White;
                    testApprovalLabel.Text = aTestInfo.Status + " For Driving License";
                }

                applyIdTextBox.Text = aApply.ApplyId.ToString();
                licenseForTextbox.Text = aApply.ApplyFor;
                issueTextBox.Text = DateTime.Now.ToString("dd/MM/yyyy");
                validityTextBox.Text = DateTime.Now.AddYears(10).ToString("dd/MM/yyyy");

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void notIssueButton_Click(object sender, EventArgs e)
        {
            string message = aManager.UpdateApplyIssue(applyId,"Not Issue");
            if (message=="Update")
            {
                Response.Redirect("BrtaOfficerUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }

        protected void issueButton_Click(object sender, EventArgs e)
        {
            DrivingLicense aDrivingLicense=new DrivingLicense();
            aDrivingLicense.DrivingLicenseId = drivingLicenseIdTextBox.Text;
            aDrivingLicense.Username = paticipant;
            aDrivingLicense.MobileNumber = mobileLabel.Text;
            aDrivingLicense.LicenseFor = licenseForTextbox.Text;
            aDrivingLicense.Issue = issueTextBox.Text;
            aDrivingLicense.Validity = validityTextBox.Text;
            aDrivingLicense.IssuedBy = Session["User"].ToString();
            aDrivingLicense.ApplyId = applyId;

            string message = aManager.SaveDrivingLicense(aDrivingLicense);
            if (message == "Save")
            {
                Response.Redirect("BrtaOfficerUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }
    }
}