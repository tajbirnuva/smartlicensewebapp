﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;

namespace SmartLicenseWebApp.UI.BRTAOfficerUI
{
    public partial class BrtaOfficerUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string username = Session["User"].ToString();
                //Load All Driving Apply
                drivingApplyGridView.DataSource = aManager.GetAllTestedApply("Driving License");
                drivingApplyGridView.DataBind();

                //Load All Vehicle Apply
                vehicleApplyGridView.DataSource = aManager.GetAllTestedApply("Vehicle License");
                vehicleApplyGridView.DataBind();
            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }


        protected void DrivingLicensesLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int applyId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("DrivingLicenseApprovalUI.aspx?applyId=" + applyId);
        }

        protected void VehicleLicensesLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int applyId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("VehicleLicenseApprovalUI.aspx?applyId=" + applyId);
        }


    }
}