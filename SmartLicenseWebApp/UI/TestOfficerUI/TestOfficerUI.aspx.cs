﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.TestOfficerUI
{
    public partial class TestOfficerUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string username = Session["User"].ToString();
                //Load All Apply
                applyReportGridView.DataSource = aManager.GetAllApply();
                applyReportGridView.DataBind();
            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (participentUsernameTextbox.Text == "" || participentUsernameTextbox.Text.Length < 11)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Enter Participant Username Properly!!!!!!!!');", true);
            }
            else if (applyIdTextBox.Text == "" ||
                     System.Text.RegularExpressions.Regex.IsMatch(applyIdTextBox.Text, "[^0-9]"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter an Valid Amount!!!!!!!');",
                    true);
            }
            else
            {
                string participantUsername = participentUsernameTextbox.Text;
                int applyId = Convert.ToInt32(applyIdTextBox.Text);
                Apply aApply = aManager.GetParticipantInApply(participantUsername, applyId);
                if (aApply != null)
                {
                    Session["ApplyId"] = applyId;
                    Session["Participant"] = participantUsername;
                    Session["LicenseType"] = aApply.LicenseType;

                    if (aApply.TestDate!="Pending")
                    {
                        if (aApply.LicenseType == "Driving License")
                        {
                            Response.Redirect("DrivingTestUI.aspx");
                        }
                        else
                        {
                            Response.Redirect("VehicleTestUI.aspx");
                        }
                    }
                    else
                    {
                        ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                            "alert('Test Date Pending Yet!!!!');", true);
                    }
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                        "alert('Participant Details Not Found In Apply List');", true);
                }
            }
        }


        protected void VerifyLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            int applyId = Convert.ToInt32(idHiddenField.Value);
            Response.Redirect("VerifyApplyLicenseUI.aspx?applyId=" + applyId);
        }

    }
}