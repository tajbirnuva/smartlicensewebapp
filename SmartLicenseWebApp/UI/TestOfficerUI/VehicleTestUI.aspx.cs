﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.TestOfficerUI
{
    public partial class VehicleTestUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        string testerUsername;
        string participantUsername;
        int applyId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                testerUsername = Session["User"].ToString();
                participantUsername = Session["Participant"].ToString();
                applyId = Convert.ToInt32(Session["ApplyId"]);
                PersonIdentity person = aManager.GetPersonDetails(participantUsername);
                //add all details
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = "Father's Name :"+person.Father;
                mobileLabel.Text = "Mobile : +88 "+person.MobileNumber;
                birthdateLabel.Text = "Date of Birth : " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group : " + person.BloodGroup;


                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);

                ApplyIdTextBox.Text ="#"+ applyId.ToString();
                applyForTextBox.Text = aManager.GetParticipantDetailsInApply(applyId).ApplyFor;

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            TestInfo aTestInfo = new TestInfo();
            aTestInfo.TesterUsername = testerUsername;
            aTestInfo.ApplyId = applyId;
            aTestInfo.TestType = "Vehicle";
            aTestInfo.QAns1 = documentDropDownList.SelectedValue;
            aTestInfo.QAns2 = vinTextbox.Text;
            aTestInfo.QAns3 = companyTextBox.Text;
            aTestInfo.QAns4 = modelTextBox.Text;
            aTestInfo.QAns5 = vehicleClassDropDownList.SelectedValue;
            aTestInfo.Status = approvalDropDownList.SelectedValue;
            aTestInfo.TestDate = DateTime.Now.ToString("dd/MM/yyyy");
            string message = aManager.SaveVehicleTestInfo(aTestInfo);
            if (message == "Save")
            {
                Response.Redirect("TestOfficerUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }
    }
}