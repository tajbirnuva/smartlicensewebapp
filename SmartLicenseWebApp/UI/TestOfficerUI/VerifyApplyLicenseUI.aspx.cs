﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.TestOfficerUI
{
    public partial class VerifyApplyLicenseUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        MedicalInfo aMedicalInfo = new MedicalInfo();
        Apply aApply=new Apply();
        private int applyId;
        protected void Page_Load(object sender, EventArgs e)
        {
            applyId = Convert.ToInt32(Request.QueryString["applyId"]);
            aMedicalInfo = aManager.GetMedicalInfoByApplyId(applyId);
            aApply = aManager.GetParticipantDetailsInApply(applyId);

            licenseTypeLabel.Text = aApply.LicenseType;

            string image1 = Convert.ToBase64String(aApply.UtilityImage);
            utilityImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image1);

            string image2 = Convert.ToBase64String(aApply.DocumentImage);
            documentImage.ImageUrl = String.Format("data:image/jpg;base64,{0}", image2);

            if (aMedicalInfo != null)
            {
                applyIdLabel.Text = "Apply ID: " + applyId;
                checkupStatusLabel.Text = "Checkup Complete";
                dateLabel.Text = "checkup Date: " + aMedicalInfo.CheckupDate;
                if (aMedicalInfo.Status == "Approve")
                {
                    approveLabel.BackColor = Color.Green;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = "Participant is " + aMedicalInfo.Status + "d For Driving License";
                }
                else
                {
                    approveLabel.BackColor = Color.Red;
                    approveLabel.ForeColor = Color.White;
                    approveLabel.Text = "You are " + aMedicalInfo.Status + " For Driving License";
                }

            }
            else
            {
                applyIdLabel.Text = "Apply ID: " + applyId;
                checkupStatusLabel.Text = "Not Checkup Yet";
            }

        }

        protected void saveButton_Click(object sender, EventArgs e)
        {
            string message =aManager.GiveDateInApply(dateTextbox.Text,applyId);
            if (message=="Save")
            {
                Response.Redirect("TestOfficerUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }
    }
}