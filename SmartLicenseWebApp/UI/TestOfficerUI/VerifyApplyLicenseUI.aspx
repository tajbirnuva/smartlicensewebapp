﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VerifyApplyLicenseUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.TestOfficerUI.VerifyApplyLicenseUI" %>

<!DOCTYPE html>


<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Verify Apply License</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <link href="../../Content/bootstrap-datepicker.css" rel="stylesheet" />

    <style>
        #utilityImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }
        #documentImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }

    </style>
    
    

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>



<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="GeneralUserUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="TestOfficerUI.aspx" class="smoothScroll">Home</a></li>
                <li class="appointment-btn"><a href="../WelcomeUI.aspx" class="smoothScroll">Logout</a></li>
            </ul>
        </div>

    </div>
</section>

    
      <form id="form2" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h3><asp:Label ID="applyIdLabel" runat="server" Text=""></asp:Label></h3>
                                  <asp:Label ID="licenseTypeLabel" runat="server" Text=""></asp:Label>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">

                                  <div class="col-md-9 col-sm-9">
                                    <h4><label for="date">Medical Checkup Info</label></h4>  
                                      
                                  </div>

                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="checkupStatusLabel" runat="server" Text=""></asp:Label>
                                  </div>
                                 
                                  

                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="dateLabel" runat="server" Text=""></asp:Label>
                                      
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <asp:Label ID="approveLabel" runat="server" Text=""></asp:Label>
                                  </div>

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              
              <div class="wow fadeInUp" data-wow-delay="0.8s">
                
                  <div class="col-md-12 col-sm-12">
                      <h4>Test Date</h4>
                  </div>
                  <div class="wow fadeInUp" data-wow-delay="0.8s">

                      <div class="col-md-9 col-sm-9">
                          <label for="date">Date</label>
                          <asp:TextBox ID="dateTextbox" class="form-control" placeholder="Enter Participant Test Date" runat="server"></asp:TextBox>
                      </div>
                     
                      <div class="col-md-6 col-sm-6">
                          <asp:Label ID="notFoundLabel" runat="server" Text=""></asp:Label>
                                     
                      </div>

                      <div class="col-md-3 col-sm-3">
                          <asp:Button ID="saveButton" class="form-control" runat="server" Text="Save" OnClick="saveButton_Click"   />
                                     
                      </div>

                  </div> 
                        </div>   
                    </div>

               </div>
          </div>

     </section>
          
          
          <section id="telephone" data-stellar-background-ratio="3">
              <div class="container">
                  <div class="row">

                      <div class="col-md-6 col-sm-6">
                 
                          <!-- SECTION TITLE -->
                          <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                              <h4>Utility Paper</h4>
                          </div>

                          <div class="wow fadeInUp" data-wow-delay="0.8s">

                              <div class="col-md-12 col-sm-12">
                                  <asp:Image ID="utilityImage" runat="server" />
                              </div>


                          </div>

                      </div>
                      <div class="col-md-6 col-sm-6">
                 
                          <!-- SECTION TITLE -->
                          <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                              <h4>Transcript/Vehicles Document</h4>
                          </div>

                          <div class="wow fadeInUp" data-wow-delay="0.8s">

                              <div class="col-md-12 col-sm-12">
                                  <asp:Image ID="documentImage" runat="server" />
                              </div>


                          </div>

                      </div>
        
                  </div>
              </div>

          </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>
    <script src="../../Scripts/bootstrap.js"></script>
    <script src="../../Scripts/bootstrap-datepicker.js"></script>
   
<script>
    $('#dateTextbox').datepicker({
        weekStart: 0,
        todayBtn: true,
        clearBtn: true,
        keyboardNavigation: false,
        daysOfWeekDisabled: "5,6",
        daysOfWeekHighlighted: "5,6",
        autoclose: true,
        todayHighlight: true
    });
</script>

</body>
</html>
