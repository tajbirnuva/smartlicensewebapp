﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.MedicalUI
{
    public partial class PatientCheckupUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        string doctorUsername;
        string patientUsername; 
        int applyId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                doctorUsername = Session["User"].ToString();
                patientUsername = Session["Patient"].ToString();
                applyId = Convert.ToInt32(Session["ApplyId"]);
                PersonIdentity person = aManager.GetPersonDetails(patientUsername);
                //add all details
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = person.Father;
                mobileLabel.Text = person.MobileNumber;
                birthdateLabel.Text = "Date of Birth: " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group: " + person.BloodGroup;


                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void SaveButton_Click(object sender, EventArgs e)
        {
            MedicalInfo aMedicalInfo=new MedicalInfo();
            aMedicalInfo.ApplyId = applyId;
            aMedicalInfo.DoctorUsername = doctorUsername;
            aMedicalInfo.QAns1 = visionDropDownList.SelectedValue;
            aMedicalInfo.QAns2 = blindnessDropDownList.SelectedValue;
            aMedicalInfo.QAns3 = hearingDropDownList.SelectedValue;
            aMedicalInfo.QAns4 = drugDropDownList.SelectedValue;
            aMedicalInfo.QAns5 = fitDropDownList.SelectedValue;
            aMedicalInfo.Status = approvalDropDownList.SelectedValue;
            aMedicalInfo.CheckupDate = DateTime.Now.ToString("dd/MM/yyyy");
            string message = aManager.SaveMedicalInfo(aMedicalInfo);
            if (message=="Save")
            {
                Response.Redirect("DoctorUI.aspx");
            }
            else
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('" + message + "');", true);
            }
        }
    }
}
 