﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;

namespace SmartLicenseWebApp.UI.MedicalUI
{
    public partial class DoctorUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string username = Session["User"].ToString();
            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }

        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            if (patientUsernameTextbox.Text == "" || patientUsernameTextbox.Text.Length < 11)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Enter Patient Username Properly!!!!!!!!');", true);
            }
            else if (applyIdTextBox.Text == "" ||
                     System.Text.RegularExpressions.Regex.IsMatch(applyIdTextBox.Text, "[^0-9]"))
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert", "alert('Enter an Valid Amount!!!!!!!');",
                    true);
            }
            else
            {
                string patientUsername = patientUsernameTextbox.Text;
                int applyId = Convert.ToInt32(applyIdTextBox.Text);
                string message = aManager.SearchPatientInApply(patientUsername, applyId);
                if (message == "Found")
                {
                    Session["ApplyId"] = applyId;
                    Session["Patient"] = patientUsername;
                    Response.Redirect("PatientCheckupUI.aspx");
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                        "alert('Patient Details Not Found In Driving License Apply');", true);

                }
            }
        }
    }
}