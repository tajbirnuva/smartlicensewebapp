﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PatientCheckupUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.MedicalUI.PatientCheckupUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Patient Checkup</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">
    <style>
.responsive {
  width: 100%;
  max-width: 300px;
  height: auto;
}
</style>

</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>


<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="../GeneralUserUI/GeneralUserUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="../MedicalUI/DoctorUI.aspx" class="smoothScroll">Home</a></li>
                <li class="appointment-btn"><a href="../WelcomeUI.aspx" class="smoothScroll">Logout</a></li>
            </ul>
        </div>

    </div>
</section>

     <section id="team" data-stellar-background-ratio="1">
          <div class="container">
               <div class="row">
                   <div class="col-md-12 col-sm-12">
                       <div class="col-md-4 col-sm-4">
                           <div>
                               <asp:Image ID="Image" runat="server" class="responsive" />
                           </div>
                       </div>
                    <div class="col-md-8 col-sm-8">
                         <div>
                              <h2 class="wow fadeInUp" data-wow-delay="0.1s"><asp:Label ID="nameLabel" runat="server" Text=""></asp:Label></h2>
                         </div>
                        
                        <div>
                           <h4> <asp:Label ID="identityLabel" runat="server" Text="" ></asp:Label></h4>
                        </div>
                        <div>
                            <asp:Label ID="fatherLabel" runat="server" Text=""></asp:Label>
                           
                        </div>
                        <div>
                            <asp:Label ID="mobileLabel" runat="server" Text=""></asp:Label>
                           
                        </div>
                        <div>
                            <asp:Label ID="birthdateLabel" runat="server" Text=""></asp:Label>
                           
                        </div>
                        <div>
                            <asp:Label ID="bloodLabel" runat="server" Text=""></asp:Label>
                           
                        </div>
                       

                    </div>


               </div>
          </div>
          </div>
     </section>
    
      <form id="form2" runat="server">
        <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h2>Checkup Details</h2>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">

                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Is there any defect of Vision?</label>
                                      <asp:DropDownList ID="visionDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Yes</asp:ListItem>
                                          <asp:ListItem>No</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Does the applicant suffer from Night Blindness?</label>
                                      <asp:DropDownList ID="blindnessDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Yes</asp:ListItem>
                                          <asp:ListItem>No</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                                  
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Is there any defect of Hearing?</label>
                                      <asp:DropDownList ID="hearingDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Yes</asp:ListItem>
                                          <asp:ListItem>No</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Does he show any evidence of being addicted to the excessive use of Alcohol/Drugs?</label>
                                      <asp:DropDownList ID="drugDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Yes</asp:ListItem>
                                          <asp:ListItem>No</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                                  
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Is he/she fit for driving license?</label>
                                      <asp:DropDownList ID="fitDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Yes</asp:ListItem>
                                          <asp:ListItem>No</asp:ListItem>
                                      </asp:DropDownList>
                                  </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Approval</label>
                                      <asp:DropDownList ID="approvalDropDownList" class="form-control" runat="server">
                                          <asp:ListItem>Select From List</asp:ListItem>
                                          <asp:ListItem>Approve</asp:ListItem>
                                          <asp:ListItem>Not Approve</asp:ListItem>
                                      </asp:DropDownList>
                                     
                                  </div>

                                  <div class="col-md-9 col-sm-9">
                                      <asp:Button ID="SaveButton" class="form-control" runat="server" Text="Save" OnClick="SaveButton_Click"/>
                                     
                                  </div>
                                  

                              </div>

        </div>
        
          <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              
              <div class="wow fadeInUp" data-wow-delay="0.8s">

                            
                        </div>   
                    </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>
