﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Gateway;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI
{
    public partial class WelcomeUI : System.Web.UI.Page
    {
        Manager aManager=new Manager();
        Users aUsers=new Users();
        protected void Page_Load(object sender, EventArgs e)
        {
            Session["User"] = null;
        }

        protected void signinButton_Click(object sender, EventArgs e)
        {
            string username = usernameTextbox.Text;
            string password = passwordTextBox.Text;
            if (username!="" && password!="")
            {
                aUsers = aManager.GetUsers(username);
                if (aUsers != null)
                {
                    if (aUsers.Username == username && aUsers.Password == password)
                    {
                        if (aUsers.Status == "General")
                        {
                            Session["User"] = aUsers.Username;
                            Response.Redirect("GeneralUserUI/GeneralUserUI.aspx");
                        }
                        else if (aUsers.Status == "Doctor")
                        {
                            Session["User"] = aUsers.Username;
                            Response.Redirect("MedicalUI/DoctorUI.aspx");
                        }
                        else if (aUsers.Status == "Test Officer")
                        {
                            Session["User"] = aUsers.Username;
                            Response.Redirect("TestOfficerUI/TestOfficerUI.aspx");
                        }
                        else if (aUsers.Status == "Senior Officer")
                        {
                            Session["User"] = aUsers.Username;
                            Response.Redirect("BRTAOfficerUI/BrtaOfficerUI.aspx");
                        }
                        else
                        {
                            Session["User"] = aUsers.Username;
                            Response.Redirect("TrafficPoliceUI/TrafficPoliceSearchUI.aspx");
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Type Username & Password Properly')", true);
                    }

                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('User Not Found')", true);
                }

            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('Fill All TextBox Properly')", true);
            }


        }

        protected void signupButton_Click(object sender, EventArgs e)
        {
            aUsers.IdentityId = identityIdTextBox.Text;
            aUsers.Username = emailTextbox.Text;
            aUsers.Password = passTextBox.Text;
            aUsers.Status = "General";
            String msg = aManager.SaveUsers(aUsers);

            if (msg== "Save")
            {
                Session["User"] = aUsers.Username; 
                Response.Redirect("GeneralUserUI/GeneralUserUI.aspx");
            }
            else
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "alertMessage", "alert('" +msg +"')", true);
            }

        }
    }
}