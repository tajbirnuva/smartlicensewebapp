﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TrafficPoliceSearchUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.TrafficPoliceUI.TrafficPoliceSearchUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>BRTA Test Officer</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">


</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>


<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="../TrafficPoliceUI/TrafficPoliceSearchUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="TrafficPoliceSearchUI.aspx" class="smoothScroll">Home</a></li>
                <li class="appointment-btn"><a href="../WelcomeUI.aspx" class="smoothScroll">Logout</a></li>
            </ul>
        </div>

    </div>
</section>

    
      <form id="form2" runat="server">
          <section id="appointment" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">
            <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                <h2>Search Details</h2>
            </div>
            <div class="col-md-6 col-sm-6">
                 
                <!-- SECTION TITLE -->

                              <div class="wow fadeInUp" data-wow-delay="0.8s">

                                  <div class="col-md-9 col-sm-9">
                                      
                                          <label for="date">Search By</label>
                                          <asp:DropDownList ID="searchOptionDropDownList" class="form-control" runat="server">
                                              <asp:ListItem>Select From List</asp:ListItem>
                                              <asp:ListItem Value="Username">Owner's ID</asp:ListItem>
                                              <asp:ListItem Value="MobileNumber">Owner's Mobile no.</asp:ListItem>
                                              <asp:ListItem Value="LicenseId">License Number</asp:ListItem>
                                          </asp:DropDownList>
                                        </div>
                                  <div class="col-md-9 col-sm-9">
                                      <label for="date">Search</label>
                                      <asp:TextBox ID="searchIdTextBox" class="form-control" placeholder="Enter Details" runat="server"></asp:TextBox>
                                  </div>
                                  <div class="col-md-6 col-sm-6">
                                      <asp:Label ID="notFoundLabel" runat="server" Text=""></asp:Label>
                                     
                                  </div>

                                  <div class="col-md-3 col-sm-3">
                                      <asp:Button ID="SearchButton" class="form-control" runat="server" Text="Search" OnClick="SearchButton_Click"  />
                                     
                                  </div>

                              </div>
            </div>
            
            
              <div class="col-md-6 col-sm-6">
                         <!-- CONTACT FORM HERE -->
              
              <div class="wow fadeInUp" data-wow-delay="0.8s">
                
                  <div class="col-md-12 col-sm-12">
                      <h4>Driving License</h4>
                      <asp:GridView ID="drivingLicenseGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />

                          <Columns>
                
                              <asp:TemplateField HeaderText="SL">
                                  <ItemTemplate>
                                      <%#Container.DataItemIndex+1 %>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("DrivingLicenseId")%>'/>
                                  </ItemTemplate>
                              </asp:TemplateField>

                              <asp:TemplateField HeaderText="License Number">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("DrivingLicenseId")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="License For">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("LicenseFor")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>

                
                              <asp:TemplateField HeaderText="Details">
                                  <ItemTemplate>
                                      <asp:LinkButton ID="showLinkButton" runat="server" OnClick="ShowDrivingLinkButton_OnClick">Show</asp:LinkButton>
                                    </ItemTemplate>
                              </asp:TemplateField>

                          </Columns>
                      </asp:GridView>
                      </div>
                  
                   <div class="col-md-12 col-sm-12">
                      <h4>Vehicle License</h4>
                      <asp:GridView ID="vehicleLicenseGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />

                          <Columns>
                
                              <asp:TemplateField HeaderText="SL">
                                  <ItemTemplate>
                                      <%#Container.DataItemIndex+1 %>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("VehicleLicenseId")%>'/>
                                  </ItemTemplate>
                              </asp:TemplateField>

                              <asp:TemplateField HeaderText="License Number">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("VehicleLicenseId")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="License For">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("VehicleType")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                
                              <asp:TemplateField HeaderText="Details">
                                  <ItemTemplate>
                                      <asp:LinkButton ID="showLinkButton" runat="server" OnClick="ShowVehicleLinkButton_OnClick">Show</asp:LinkButton>
                                    </ItemTemplate>
                              </asp:TemplateField>

                          </Columns>
                      </asp:GridView>
                      </div>

              </div>   
                    </div>  
            
            

        
               </div>
          </div>
          </section>

         
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>