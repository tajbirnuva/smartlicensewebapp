﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.UI.TrafficPoliceUI
{
    public partial class DrivingLicenseUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();
        string licenseId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                licenseId = Request.QueryString["licenseId"];
                DrivingLicense aLicense = aManager.GetDrivingLicenseByLicenseNumber(licenseId);

                //Participant details
                PersonIdentity person = aManager.GetPersonDetails(aLicense.Username);
                nameLabel.Text = person.Name;
                identityLabel.Text = person.IdentityId;
                fatherLabel.Text = person.Father;
                mobileLabel.Text = person.MobileNumber;
                birthdateLabel.Text = "Date of Birth: " + person.DateOfBirth;
                bloodLabel.Text = "Blood Group: " + person.BloodGroup;

                string personImage = Convert.ToBase64String(person.Image);
                Image.ImageUrl = String.Format("data:image/jpg;base64,{0}", personImage);

                // License Details
                licenseIdLabel.Text = "License ID: " + aLicense.DrivingLicenseId;
                licenseForLabel.Text = "- " + aLicense.LicenseFor;
                issueLabel.Text = "Issue :  " + aLicense.Issue;
                validityLabel.Text = "Validity :  " + aLicense.Validity;
                issueByLabel.Text = "Issued By :  " + aLicense.IssuedBy;


                //Offense Details
                offenseGridView.DataSource = aManager.GetLicenseOffenses(licenseId);
                offenseGridView.DataBind();
                //License Status

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void offenseButton_Click(object sender, EventArgs e)
        {
            if (offenseDropDownList.SelectedValue== "Select From List")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Select an Offense!!!!');", true);
            }
            else
            {
                Offense aOffense=new Offense();
                aOffense.OffenseType = offenseDropDownList.SelectedValue;
                aOffense.LicenseId = licenseId;
                aOffense.IssuedBy = Session["User"].ToString();
                aOffense.IssueDate= DateTime.Now.ToString("dd/MM/yyyy");
                string message = aManager.SaveOffense(aOffense);
                if (message=="Save")
                {
                    offenseGridView.DataSource = aManager.GetLicenseOffenses(licenseId);
                    offenseGridView.DataBind();
                }
                else
                {
                    ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                        "alert('"+message+"');", true);
                }

            }
        }
    }
}