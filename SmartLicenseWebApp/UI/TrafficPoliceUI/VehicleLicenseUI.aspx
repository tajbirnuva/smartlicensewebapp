﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="VehicleLicenseUI.aspx.cs" Inherits="SmartLicenseWebApp.UI.TrafficPoliceUI.VehicleLicenseUI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vehicle License</title>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="author" content="Tooplate">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <link rel="stylesheet" href="../../css/bootstrap.min.css">
    <link rel="stylesheet" href="../../css/font-awesome.min.css">
    <link rel="stylesheet" href="../../css/animate.css">
    <link rel="stylesheet" href="../../css/owl.carousel.css">
    <link rel="stylesheet" href="../../css/owl.theme.default.min.css">

    <!-- MAIN CSS -->
    <link rel="stylesheet" href="../../css/tooplate-style.css">
    
    <style>
        .responsive {
            width: 100%;
            max-width: 300px;
            height: auto;
            border: 2px solid #555;
        }
        #utilityImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }
        #documentImage {
            width: 100%;
            height: 100%;
            background-repeat: no-repeat;
            background-size: cover;
            border: 2px solid #555;
        }

    </style>

</head>

<body id="top" data-spy="scroll" data-target=".navbar-collapse" data-offset="50">

<!-- PRE LOADER -->
<section class="preloader">
    <div class="spinner">

        <span class="spinner-rotate"></span>
               
    </div>
</section>


<!-- MENU -->
<section class="navbar navbar-default navbar-static-top" role="navigation">
    <div class="container">

        <div class="navbar-header">
            <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
                <span class="icon icon-bar"></span>
            </button>

            <!-- lOGO TEXT HERE -->
            <a href="TrafficPoliceSearchUI.aspx" class="navbar-brand"><i class="fas fa-car" style="font-size:40px;color:black"></i> Smart License <i class="fas fa-motorcycle"></i></a>
        </div>

        <!-- MENU LINKS -->
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="TrafficPoliceSearchUI.aspx" class="smoothScroll">Home</a></li>

            </ul>
        </div>

    </div>
</section>

<section id="team" data-stellar-background-ratio="1">
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-sm-12">
                <div class="col-md-4 col-sm-4">
                    <div>
                        <asp:Image ID="Image" runat="server" class="responsive" />
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div>
                        <h4 class="wow fadeInUp" data-wow-delay="0.1s"><asp:Label ID="nameLabel" runat="server" Text=""></asp:Label></h4>
                    </div>
                        
                    <div>
                        <h5> <asp:Label ID="identityLabel" runat="server" Text="" ></asp:Label></h5>
                    </div>
                    <div>
                        <asp:Label ID="fatherLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="mobileLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="birthdateLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                    <div>
                        <asp:Label ID="bloodLabel" runat="server" Text=""></asp:Label>
                           
                    </div>
                       

                </div>
                <div class="col-md-4 col-sm-4">
                    <div>
                        <h4 class="wow fadeInUp" data-wow-delay="0.1s"><asp:Label ID="licenseIdLabel" runat="server" Text=""></asp:Label></h4>
                    </div>
                    <div>
                        <h5> <asp:Label ID="vinLabel" runat="server" Text="" ></asp:Label></h5>
                    </div>
                    <div>
                        <asp:Label ID="vehicleLabel" runat="server" Text=""></asp:Label>
                    </div>
                    
                    <div>
                        <asp:Label ID="issueLabel" runat="server" Text=""></asp:Label>
                    </div>
                    <div>
                        <asp:Label ID="validityLabel" runat="server" Text=""></asp:Label> 
                    </div>
                    <div>
                        <asp:Label ID="issueByLabel" runat="server" Text=""></asp:Label> 
                    </div>
                    <div> <label for="date">___________________</label></div>

                    <div>
                        <asp:Label ID="statusLabel" runat="server" Text=""></asp:Label> 
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<form id="form2" runat="server">
       <section id="news" data-stellar-background-ratio="3">
        <div class="container">
        <div class="row">

        <div class="col-md-12 col-sm-12">
                 
                <!-- SECTION TITLE -->
                              <div class="section-title wow fadeInUp" data-wow-delay="0.4s">
                                   <h3>Offense Details</h3>
                              </div>

                              <div class="wow fadeInUp" data-wow-delay="0.8s">
                                  <div class="col-md-6 col-sm-6">
                                      <div class="about-info">
                                          <div class="wow fadeInUp" data-wow-delay="0.8s">
                                              <div class="col-md-12 col-sm-12">
                                                  <label for="email">Offense Type</label>
                                                  <asp:DropDownList ID="offenseDropDownList" class="form-control" runat="server">
                                                      <asp:ListItem>Select From List</asp:ListItem>
                                                      <asp:ListItem>Fitness Issue</asp:ListItem>
                                                      <asp:ListItem>Wrong Parking</asp:ListItem>
                                                  </asp:DropDownList>
                                              </div>
                                             

                                              <div class="col-md-12 col-sm-12">
                                                  <label for="telephone"></label>
                                                  <asp:Button ID="offenseButton" class="form-control" runat="server" Text="Reg. an Offence" OnClick="offenseButton_Click"  />
                                     
                                              </div>
                                              
                                          </div>
                                      </div>
                                  </div>

                                     <div class="col-md-6 col-sm-6">
                                       
                                      <div class="about-info">
                                          <div class="wow fadeInUp" data-wow-delay="0.8s">
                                             
                                              <div class="col-md-12 col-sm-12">
                                                  <h4>Offenses List</h4>
                      <asp:GridView ID="offenseGridView" runat="server" BackColor="White" BorderColor="#336666" BorderStyle="Double" BorderWidth="3px" CellPadding="4" GridLines="Horizontal" Width="100%" AutoGenerateColumns="False">
                          <FooterStyle BackColor="White" ForeColor="#333333" />
                          <HeaderStyle BackColor="#336666" Font-Bold="True" ForeColor="White" />
                          <PagerStyle BackColor="#336666" ForeColor="White" HorizontalAlign="Center" />
                          <RowStyle BackColor="White" ForeColor="#333333" />
                          <SelectedRowStyle BackColor="#339966" Font-Bold="True" ForeColor="White" />
                          <SortedAscendingCellStyle BackColor="#F7F7F7" />
                          <SortedAscendingHeaderStyle BackColor="#487575" />
                          <SortedDescendingCellStyle BackColor="#E5E5E5" />
                          <SortedDescendingHeaderStyle BackColor="#275353" />

                          <Columns>
                
                              <asp:TemplateField HeaderText="SL">
                                  <ItemTemplate>
                                      <%#Container.DataItemIndex+1 %>
                                      <asp:HiddenField ID="idHiddenField" runat="server" Value='<%#Eval("OffenseId")%>'/>
                                  </ItemTemplate>
                              </asp:TemplateField>

                              <asp:TemplateField HeaderText="Offense ID">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("OffenseId")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Offense Type">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("OffenseType")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Date">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("IssueDate")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>
                              <asp:TemplateField HeaderText="Status">
                                  <ItemTemplate>
                                      <asp:Label runat="server" Text='<%#Eval("Status")%>' ></asp:Label>
                                  </ItemTemplate>
                              </asp:TemplateField>

                          </Columns>
                      </asp:GridView>
                      </div>
                                             

                                          </div>
                                      </div>
                                  </div>
                                  

                              </div>

        </div>

               </div>
          </div>

     </section>
      </form>

<!-- SCRIPTS -->
<script src="../../js/jquery.js"></script>
<script src="../../js/bootstrap.min.js"></script>
<script src="../../js/jquery.sticky.js"></script>
<script src="../../js/jquery.stellar.min.js"></script>
<script src="../../js/wow.min.js"></script>
<script src="../../js/smoothscroll.js"></script>
<script src="../../js/owl.carousel.min.js"></script>
<script src="../../js/custom.js"></script>
<script src='https://kit.fontawesome.com/a076d05399.js'></script>

</body>
</html>


