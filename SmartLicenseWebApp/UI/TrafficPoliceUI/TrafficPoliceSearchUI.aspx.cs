﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SmartLicenseWebApp.BLL;

namespace SmartLicenseWebApp.UI.TrafficPoliceUI
{
    public partial class TrafficPoliceSearchUI : System.Web.UI.Page
    {
        Manager aManager = new Manager();

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["User"] != null)
            {
                string username = Session["User"].ToString();

            }
            else
            {
                Response.Redirect("../WelcomeUI.aspx");
            }
        }

        protected void SearchButton_Click(object sender, EventArgs e)
        {
            string searchType = searchOptionDropDownList.SelectedValue;
            string searchId = searchIdTextBox.Text;
            if (searchType == "Select From List")
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Please Select Search Type!!!!!');", true);
            }
            else if (searchId=="" || searchId.Length<10)
            {
                ClientScript.RegisterStartupScript(this.GetType(), "myalert",
                    "alert('Type Mobile Number/Username/License Id Properly!!!!');", true);

            }
            else
            {
                drivingLicenseGridView.DataSource = aManager.GetDrivingLicense(searchType, searchId);
                drivingLicenseGridView.DataBind();

                vehicleLicenseGridView.DataSource = aManager.GetVehicleLicense(searchType, searchId);
                vehicleLicenseGridView.DataBind();
            }

        }



        protected void ShowDrivingLinkButton_OnClick(object sender, EventArgs e)
        {
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            string licenseId = idHiddenField.Value;
            Response.Redirect("DrivingLicenseUI.aspx?licenseId=" + licenseId);
        }



        protected void ShowVehicleLinkButton_OnClick(object sender, EventArgs e)
        { 
            LinkButton updateLinkButton = (LinkButton)sender;
            DataControlFieldCell cell = (DataControlFieldCell)updateLinkButton.Parent;
            GridViewRow row = (GridViewRow)cell.Parent;
            HiddenField idHiddenField = (HiddenField)row.FindControl("idHiddenField");
            string licenseId = idHiddenField.Value;
            Response.Redirect("VehicleLicenseUI.aspx?licenseId=" + licenseId);
        }

    }
}