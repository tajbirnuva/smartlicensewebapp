﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using SmartLicenseWebApp.DAL.Model;

namespace SmartLicenseWebApp.DAL.Gateway
{
    public class Gateway : BaseGateway
    {
        public bool PersonExist(string identityId)
        {
            string query = "SELECT * FROM PersonIdentity WHERE IdentityId=@IdentityId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@IdentityId", identityId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public PersonIdentity GetPersonIdentity(string identityId)
        {
            string query = "SELECT * From PersonIdentity WHERE IdentityId=@IdentityId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@IdentityId", identityId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            PersonIdentity identity = new PersonIdentity();
            Reader.Read();

            identity.IdentityId = Reader["IdentityId"].ToString();
            identity.Name = Reader["Name"].ToString();
            identity.Father = Reader["Father"].ToString();
            identity.Mother = Reader["Mother"].ToString();
            identity.DateOfBirth = Reader["DateOfBirth"].ToString();
            identity.Sex = Reader["Sex"].ToString();
            identity.MobileNumber = Reader["MobileNumber"].ToString();
            identity.BloodGroup = Reader["BloodGroup"].ToString();
            identity.PermanentAddress = Reader["PermanentAddress"].ToString();
            identity.PlaceOfBirth = Reader["PlaceOfBirth"].ToString();
            identity.Type = Reader["Type"].ToString();
            identity.Nationality = Reader["Nationality"].ToString();
            identity.Image = (byte[])Reader["Image"];

            Connection.Close();
            return identity;
        }

        public bool UserExistByIndentityId(Users aUsers)
        {
            string query = "SELECT * FROM Users WHERE IdentityId=@IdentityId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@IdentityId", aUsers.IdentityId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public bool UserExistByUsername(string username)
        {
            string query = "SELECT * FROM Users WHERE Username=@Username";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", username);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public int SaveUsers(Users aUser)
        {
            string query = "INSERT INTO Users(Username,Password,IdentityId,Status) VALUES (@Username,@Password,@IdentityId,@Status)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", aUser.Username);
            Command.Parameters.AddWithValue("@Password", aUser.Password);
            Command.Parameters.AddWithValue("@IdentityId", aUser.IdentityId);
            Command.Parameters.AddWithValue("@Status", aUser.Status);


            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public Users GetUserByUsername(string username)
        {
            string query = "SELECT * From Users WHERE Username=@Username";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", username);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Users aUsers = new Users();
            Reader.Read();

            aUsers.Username = Reader["Username"].ToString();
            aUsers.Password = Reader["Password"].ToString();
            aUsers.IdentityId = Reader["IdentityId"].ToString();
            aUsers.Status = Reader["Status"].ToString();

            Connection.Close();
            return aUsers;
        }

        public bool BkashNumberExist(string bkashNumber)
        {
            string query = "SELECT * FROM Bkash WHERE Number=@Number";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Number", bkashNumber);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public Bkash GetBkashInfo(string bkashNumber)
        {
            string query = "SELECT * From Bkash WHERE Number=@Number";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Number", bkashNumber);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Bkash aBkash = new Bkash();
            Reader.Read();

            aBkash.Number = Reader["Number"].ToString();
            aBkash.Pin = Reader["Pin"].ToString();
            aBkash.Amount = Convert.ToInt32(Reader["Amount"]);

            Connection.Close();
            return aBkash;
        }

        public int SaveIdentity(PersonIdentity aIdentity)
        {
            string query = "INSERT INTO PersonIdentity(IdentityId,Name,Father,Mother,Sex,DateOfBirth,MobileNumber,PlaceOfBirth,BloodGroup,Type,PermanentAddress,Nationality,Image) " +
                           "VALUES(@IdentityId,@Name,@Father,@Mother,@Sex,@DateOfBirth,@MobileNumber,@PlaceOfBirth,@BloodGroup,@Type,@PermanentAddress,@Nationality,@Image)";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@IdentityId", aIdentity.IdentityId);
            Command.Parameters.AddWithValue("@Name", aIdentity.Name);
            Command.Parameters.AddWithValue("@Father", aIdentity.Father);
            Command.Parameters.AddWithValue("@Mother", aIdentity.Mother);
            Command.Parameters.AddWithValue("@Sex", aIdentity.Sex);
            Command.Parameters.AddWithValue("@DateOfBirth", aIdentity.DateOfBirth);
            Command.Parameters.AddWithValue("@MobileNumber", aIdentity.MobileNumber);
            Command.Parameters.AddWithValue("@PlaceOfBirth", aIdentity.PlaceOfBirth);
            Command.Parameters.AddWithValue("@BloodGroup", aIdentity.BloodGroup);
            Command.Parameters.AddWithValue("@Type", aIdentity.Type);
            Command.Parameters.AddWithValue("@PermanentAddress", aIdentity.PermanentAddress);
            Command.Parameters.AddWithValue("@Nationality", aIdentity.Nationality);
            Command.Parameters.AddWithValue("@Image", aIdentity.Image);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public int UpdateBkashAmount(string bkashNumber, int newAmmount)
        {
            string query = "UPDATE Bkash SET Amount = @Amount WHERE Number=@Number";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Amount", newAmmount);
            Command.Parameters.AddWithValue("@Number", bkashNumber);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public int SaveApplyInfo(Apply aApply)
        {
            string query = "INSERT INTO Apply(Username,LicenseType,ApplyFor,Payment,UtilityImage,DocumentImage) VALUES (@Username,@LicenseType,@ApplyFor,@Payment,@UtilityImage,@DocumentImage)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", aApply.Username);
            Command.Parameters.AddWithValue("@LicenseType", aApply.LicenseType);
            Command.Parameters.AddWithValue("@ApplyFor", aApply.ApplyFor);
            Command.Parameters.AddWithValue("@Payment", "Payment Clear");
            Command.Parameters.AddWithValue("@UtilityImage", aApply.UtilityImage);
            Command.Parameters.AddWithValue("@DocumentImage", aApply.DocumentImage);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public List<Apply> GetAllApplyDetails(string username)
        {
            string query = "SELECT * FROM Apply WHERE Username=@Username";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", username);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Apply> applyList = new List<Apply>();
            while (Reader.Read())
            {
                Apply aApply = new Apply();

                aApply.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
                aApply.LicenseType = Reader["LicenseType"].ToString();
                aApply.ApplyFor = Reader["ApplyFor"].ToString();
                aApply.IsIssued = Reader["IsIssued"].ToString();
                aApply.TestDate = Reader["TestDate"].ToString();

                applyList.Add(aApply);
            }

            Connection.Close();

            return applyList;
        }

        public bool SearchPatientInApply(string patientUsername, int applyId)
        {
            string query = "SELECT * FROM Apply WHERE Username=@Username AND ApplyId=@ApplyId AND LicenseType=@LicenseType AND IsIssued=@IsIssued";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@Username", patientUsername);
            Command.Parameters.AddWithValue("@ApplyId", applyId);
            Command.Parameters.AddWithValue("@LicenseType", "Driving License");
            Command.Parameters.AddWithValue("@IsIssued", "Pending");

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public int SaveMedicalInfo(MedicalInfo aInfo)
        {
            string query = "INSERT INTO MedicalInfo(ApplyId,QAns1,QAns2,QAns3,QAns4,QAns5,DoctorUsername,Status,CheckupDate) VALUES " +
                           "(@ApplyId,@QAns1,@QAns2,@QAns3,@QAns4,@QAns5,@DoctorUsername,@Status,@CheckupDate)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ApplyId", aInfo.ApplyId);
            Command.Parameters.AddWithValue("@QAns1", aInfo.QAns1);
            Command.Parameters.AddWithValue("@QAns2", aInfo.QAns2);
            Command.Parameters.AddWithValue("@QAns3", aInfo.QAns3);
            Command.Parameters.AddWithValue("@QAns4", aInfo.QAns4);
            Command.Parameters.AddWithValue("@QAns5", aInfo.QAns5);
            Command.Parameters.AddWithValue("@DoctorUsername", aInfo.DoctorUsername);
            Command.Parameters.AddWithValue("@Status", aInfo.Status);
            Command.Parameters.AddWithValue("@CheckupDate", aInfo.CheckupDate);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public bool MedicalInfoExist(int applyId)
        {
            string query = "SELECT * FROM MedicalInfo WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public MedicalInfo GetMedicalInfoByApplyId(int applyId)
        {
            string query = "SELECT * FROM MedicalInfo WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            MedicalInfo aInfo = new MedicalInfo();
            Reader.Read();

            aInfo.MedicalInfoId = Convert.ToInt32(Reader["MedicalInfoId"]);
            aInfo.ApplyId = Convert.ToInt32(Reader["ApplyID"]);
            aInfo.QAns1 = Reader["QAns1"].ToString();
            aInfo.QAns2 = Reader["QAns2"].ToString();
            aInfo.QAns3 = Reader["QAns3"].ToString();
            aInfo.QAns4 = Reader["QAns4"].ToString();
            aInfo.QAns5 = Reader["QAns5"].ToString();
            aInfo.DoctorUsername = Reader["DoctorUsername"].ToString();
            aInfo.Status = Reader["Status"].ToString();
            aInfo.CheckupDate = Reader["CheckupDate"].ToString();

            Connection.Close();
            return aInfo;
        }

        public bool ParticipantExistInApply(string participantUsername, int applyId)
        {
            string query = "SELECT * FROM Apply WHERE ApplyId=@ApplyId AND Username=@Username";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@Username", participantUsername);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public Apply GetParticipantInApply(string participantUsername, int applyId)
        {
            string query = "SELECT * FROM Apply WHERE ApplyId=@ApplyId AND Username=@Username";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@Username", participantUsername);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Apply aApply = new Apply();
            Reader.Read();

            aApply.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
            aApply.ApplyFor = Reader["ApplyFor"].ToString();
            aApply.LicenseType = Reader["LicenseType"].ToString();
            aApply.TestDate = Reader["TestDate"].ToString();

            Connection.Close();
            return aApply;
        }

        public int SaveTestInfo(TestInfo aTestInfo)
        {
            string query = "INSERT INTO TestInfo(ApplyId,TestType,QAns1,QAns2,QAns3,QAns4,QAns5,TesterUsername,Status,TestDate) VALUES " +
                           "(@ApplyId,@TestType,@QAns1,@QAns2,@QAns3,@QAns4,@QAns5,@TesterUsername,@Status,@TestDate)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ApplyId", aTestInfo.ApplyId);
            Command.Parameters.AddWithValue("@TestType", aTestInfo.TestType);
            Command.Parameters.AddWithValue("@QAns1", aTestInfo.QAns1);
            Command.Parameters.AddWithValue("@QAns2", aTestInfo.QAns2);
            Command.Parameters.AddWithValue("@QAns3", aTestInfo.QAns3);
            Command.Parameters.AddWithValue("@QAns4", aTestInfo.QAns4);
            Command.Parameters.AddWithValue("@QAns5", aTestInfo.QAns5);
            Command.Parameters.AddWithValue("@TesterUsername", aTestInfo.TesterUsername);
            Command.Parameters.AddWithValue("@Status", aTestInfo.Status);
            Command.Parameters.AddWithValue("@TestDate", aTestInfo.TestDate);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public List<Apply> GetAllApply()
        {
            string query = "SELECT * FROM Apply WHERE TestDate=@TestDate";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@TestDate", "Pending");

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Apply> applyList = new List<Apply>();
            while (Reader.Read())
            {
                Apply aApply = new Apply();

                aApply.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
                aApply.LicenseType = Reader["LicenseType"].ToString();
                aApply.Username = Reader["Username"].ToString();
                aApply.ApplyFor = Reader["ApplyFor"].ToString();
                aApply.IsIssued = Reader["IsIssued"].ToString();
                aApply.TestDate = Reader["TestDate"].ToString();

                applyList.Add(aApply);
            }

            Connection.Close();

            return applyList;
        }

        public Apply GetParticipantDetailsInApply(int applyId)
        {
            string query = "SELECT * FROM Apply WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            Apply aApply = new Apply();
            Reader.Read();

            aApply.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
            aApply.Username = Reader["Username"].ToString();
            aApply.ApplyFor = Reader["ApplyFor"].ToString();
            aApply.LicenseType = Reader["LicenseType"].ToString();
            aApply.TestDate = Reader["TestDate"].ToString();
            aApply.UtilityImage = (byte[])Reader["UtilityImage"];
            aApply.DocumentImage = (byte[])Reader["DocumentImage"];

            Connection.Close();
            return aApply;
        }

        public int GiveDateInApply(string date, int applyId)
        {
            string query = "UPDATE Apply SET TestDate = @TestDate WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@TestDate", date);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public bool TestInfoExistByApplyId(int applyId)
        {
            string query = "SELECT * FROM TestInfo WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public TestInfo GetTestInfoByApplyId(int applyId)
        {
            string query = "SELECT * FROM TestInfo WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            TestInfo aTestInfo = new TestInfo();
            Reader.Read();

            aTestInfo.TestInfoId = Convert.ToInt32(Reader["TestInfoId"]);
            aTestInfo.ApplyId = Convert.ToInt32(Reader["ApplyID"]);
            aTestInfo.TestType = Reader["TestType"].ToString();
            aTestInfo.QAns1 = Reader["QAns1"].ToString();
            aTestInfo.QAns2 = Reader["QAns2"].ToString();
            aTestInfo.QAns3 = Reader["QAns3"].ToString();
            aTestInfo.QAns4 = Reader["QAns4"].ToString();
            aTestInfo.QAns5 = Reader["QAns5"].ToString();
            aTestInfo.TesterUsername = Reader["TesterUsername"].ToString();
            aTestInfo.Status = Reader["Status"].ToString();
            aTestInfo.TestDate = Reader["TestDate"].ToString();

            Connection.Close();
            return aTestInfo;
        }

        public bool VinInfoExistByVinId(string vinId)
        {
            string query = "SELECT * FROM TestInfo WHERE QAns2=@QAns2";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@QAns2", vinId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public List<Apply> GetAllTestedApply(string licenseType)
        {
            string query;
            if (licenseType == "Driving License")
            {
                query = "EXEC DrivingLicensesNotIssuedYet";
            }
            else
            {
                query = "EXEC VehicleLicensesNotIssuedYet";
            }

            Command = new SqlCommand(query, Connection);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Apply> applyList = new List<Apply>();
            while (Reader.Read())
            {
                Apply aApply = new Apply();
                aApply.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
                aApply.Username = Reader["Username"].ToString();
                aApply.LicenseType = Reader["LicenseType"].ToString();
                aApply.ApplyFor = Reader["ApplyFor"].ToString();

                applyList.Add(aApply);
            }

            Connection.Close();

            return applyList;
        }

        public int UpdateApplyIssue(int applyId, string isIssue)
        {
            string query = "UPDATE Apply SET IsIssued = @IsIssued WHERE ApplyId=@ApplyId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@IsIssued", isIssue);
            Command.Parameters.AddWithValue("@ApplyId", applyId);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public bool DrivingLicenseExist(string drivingLicenseId)
        {
            string query = "SELECT * FROM DrivingLicense WHERE DrivingLicenseId=@DrivingLicenseId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@DrivingLicenseId", drivingLicenseId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public int SaveDrivingLicense(DrivingLicense aDrivingLicense)
        {
            string query = "INSERT INTO DrivingLicense(DrivingLicenseId,Username,MobileNumber,LicenseFor,Issue,Validity,IssuedBy,ApplyId) VALUES (@DrivingLicenseId,@Username,@MobileNumber,@LicenseFor,@Issue,@Validity,@IssuedBy,@ApplyId)";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@DrivingLicenseId", aDrivingLicense.DrivingLicenseId);
            Command.Parameters.AddWithValue("@Username", aDrivingLicense.Username);
            Command.Parameters.AddWithValue("@MobileNumber", aDrivingLicense.MobileNumber);
            Command.Parameters.AddWithValue("@LicenseFor", aDrivingLicense.LicenseFor);
            Command.Parameters.AddWithValue("@Issue", aDrivingLicense.Issue);
            Command.Parameters.AddWithValue("@Validity", aDrivingLicense.Validity);
            Command.Parameters.AddWithValue("@IssuedBy", aDrivingLicense.IssuedBy);
            Command.Parameters.AddWithValue("@ApplyId", aDrivingLicense.ApplyId);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public bool VehicleLicenseExist(string vehicleLicenseId)
        {
            string query = "SELECT * FROM VehicleLicense WHERE VehicleLicenseId=@VehicleLicenseId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@VehicleLicenseId", vehicleLicenseId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            bool exist = Reader.HasRows;
            Connection.Close();

            return exist;
        }

        public int SaveVehicleLicense(VehicleLicense aVehicleLicense)
        {
            string query = "INSERT INTO VehicleLicense(VehicleLicenseId,Username,MobileNumber,Vehicle,Vin,VehicleType,Issue,Validity,IssuedBy,ApplyId) VALUES" +
                           " (@VehicleLicenseId,@Username,@MobileNumber,@Vehicle,@Vin,@VehicleType,@Issue,@Validity,@IssuedBy,@ApplyId)";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@VehicleLicenseId", aVehicleLicense.VehicleLicenseId);
            Command.Parameters.AddWithValue("@Username", aVehicleLicense.Username);
            Command.Parameters.AddWithValue("@MobileNumber", aVehicleLicense.MobileNumber);
            Command.Parameters.AddWithValue("@Vehicle", aVehicleLicense.Vehicle);
            Command.Parameters.AddWithValue("@Vin", aVehicleLicense.Vin);
            Command.Parameters.AddWithValue("@VehicleType", aVehicleLicense.VehicleType);
            Command.Parameters.AddWithValue("@Issue", aVehicleLicense.Issue);
            Command.Parameters.AddWithValue("@Validity", aVehicleLicense.Validity);
            Command.Parameters.AddWithValue("@IssuedBy", aVehicleLicense.IssuedBy);
            Command.Parameters.AddWithValue("@ApplyId", aVehicleLicense.ApplyId);

            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public List<DrivingLicense> GetDrivingLicense(string searchType, string searchId)
        {
            string query = "SELECT * FROM DrivingLicense WHERE " + searchType + "=@SearchType";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@SearchType", searchId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<DrivingLicense> drivingLicenseList = new List<DrivingLicense>();
            while (Reader.Read())
            {
                DrivingLicense aDrivingLicense=new DrivingLicense();

                aDrivingLicense.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
                aDrivingLicense.DrivingLicenseId = Reader["DrivingLicenseId"].ToString();
                aDrivingLicense.Username = Reader["Username"].ToString();
                aDrivingLicense.MobileNumber = Reader["MobileNumber"].ToString();
                aDrivingLicense.LicenseFor = Reader["LicenseFor"].ToString();
                aDrivingLicense.Issue = Reader["Issue"].ToString();
                aDrivingLicense.Validity = Reader["Validity"].ToString();
                aDrivingLicense.IssuedBy = Reader["IssuedBy"].ToString();

                drivingLicenseList.Add(aDrivingLicense);
            }

            Connection.Close();

            return drivingLicenseList;
        }

        public List<VehicleLicense> GetVehicleLicense(string searchType, string searchId)
        {
            string query = "SELECT * FROM VehicleLicense WHERE " + searchType + "=@SearchType";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@SearchType", searchId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<VehicleLicense> vehicleLicenseList = new List<VehicleLicense>();
            while (Reader.Read())
            {
                VehicleLicense aVehicleLicense = new VehicleLicense();

                aVehicleLicense.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
                aVehicleLicense.VehicleLicenseId = Reader["VehicleLicenseId"].ToString();
                aVehicleLicense.Username = Reader["Username"].ToString();
                aVehicleLicense.MobileNumber = Reader["MobileNumber"].ToString();
                aVehicleLicense.VehicleType = Reader["VehicleType"].ToString();
                aVehicleLicense.Vehicle = Reader["Vehicle"].ToString();
                aVehicleLicense.Vin = Reader["Vin"].ToString();
                aVehicleLicense.Issue = Reader["Issue"].ToString();
                aVehicleLicense.Validity = Reader["Validity"].ToString();
                aVehicleLicense.IssuedBy = Reader["IssuedBy"].ToString();

                vehicleLicenseList.Add(aVehicleLicense);
            }

            Connection.Close();

            return vehicleLicenseList;
        }

        public DrivingLicense GetDrivingLicenseByLicenseNumber(string licenseId)
        {
            string query = "SELECT * FROM DrivingLicense WHERE DrivingLicenseId=@DrivingLicenseId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@DrivingLicenseId", licenseId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            DrivingLicense aLicense=new DrivingLicense();
            Reader.Read();

            aLicense.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
            aLicense.DrivingLicenseId = Reader["DrivingLicenseId"].ToString();
            aLicense.Username = Reader["Username"].ToString();
            aLicense.MobileNumber = Reader["MobileNumber"].ToString();
            aLicense.LicenseFor = Reader["LicenseFor"].ToString();
            aLicense.Issue = Reader["Issue"].ToString();
            aLicense.Validity = Reader["Validity"].ToString();
            aLicense.IssuedBy = Reader["IssuedBy"].ToString();

            Connection.Close();
            return aLicense;
        }

        public int SaveOffense(Offense aOffense)
        {
            string query = "INSERT INTO Offense(OffenseType,LicenseId,IssuedBy,IssueDate) VALUES (@OffenseType,@LicenseId,@IssuedBy,@IssueDate)";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@OffenseType", aOffense.OffenseType);
            Command.Parameters.AddWithValue("@LicenseId", aOffense.LicenseId);
            Command.Parameters.AddWithValue("@IssuedBy", aOffense.IssuedBy);
            Command.Parameters.AddWithValue("@IssueDate", aOffense.IssueDate);
            
            Connection.Open();
            int rawAffect = Command.ExecuteNonQuery();
            Connection.Close();

            return rawAffect;
        }

        public List<Offense> GetLicenseOffenses(string licenseId)
        {
            string query = "SELECT * FROM Offense WHERE LicenseId=@LicenseId";
            Command = new SqlCommand(query, Connection);
            Command.Parameters.AddWithValue("@LicenseId", licenseId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            List<Offense> aOffenseList = new List<Offense>();
            while (Reader.Read())
            {
               Offense aOffense=new Offense();

               aOffense.OffenseId = Convert.ToInt32(Reader["OffenseId"]);
               aOffense.OffenseType = Reader["OffenseType"].ToString();
               aOffense.LicenseId = Reader["LicenseId"].ToString();
               aOffense.IssuedBy = Reader["IssuedBy"].ToString();
               aOffense.IssueDate = Reader["IssueDate"].ToString();
               aOffense.Status = Reader["Status"].ToString();

               aOffenseList.Add(aOffense);
            }

            Connection.Close();

            return aOffenseList;
        }

        public VehicleLicense GetVehicleLicenseByLicenseNumber(string licenseId)
        {

            string query = "SELECT * FROM VehicleLicense WHERE VehicleLicenseId=@VehicleLicenseId";
            Command = new SqlCommand(query, Connection);

            Command.Parameters.AddWithValue("@VehicleLicenseId", licenseId);

            Connection.Open();
            Reader = Command.ExecuteReader();
            VehicleLicense aLicense = new VehicleLicense();
            Reader.Read();

            aLicense.ApplyId = Convert.ToInt32(Reader["ApplyId"]);
            aLicense.VehicleLicenseId = Reader["VehicleLicenseId"].ToString();
            aLicense.Username = Reader["Username"].ToString();
            aLicense.MobileNumber = Reader["MobileNumber"].ToString();
            aLicense.Vehicle = Reader["Vehicle"].ToString();
            aLicense.VehicleType = Reader["VehicleType"].ToString();
            aLicense.Vin = Reader["Vin"].ToString();
            aLicense.Issue = Reader["Issue"].ToString();
            aLicense.Validity = Reader["Validity"].ToString();
            aLicense.IssuedBy = Reader["IssuedBy"].ToString();

            Connection.Close();
            return aLicense;
        }
    }
}