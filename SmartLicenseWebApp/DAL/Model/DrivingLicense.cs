﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class DrivingLicense
    {
        public string DrivingLicenseId { get; set; }
        public int ApplyId { get; set; }
        public string Username { get; set; }
        public string MobileNumber { get; set; }
        public string LicenseFor { get; set; }
        public string Issue { get; set; }
        public string Validity { get; set; }
        public string IssuedBy { get; set; }
    }
}