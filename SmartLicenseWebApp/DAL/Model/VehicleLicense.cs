﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class VehicleLicense
    {
        public string VehicleLicenseId { get; set; }
        public string Username { get; set; }
        public string MobileNumber { get; set; }
        public string Vehicle { get; set; }
        public string Vin { get; set; }
        public string VehicleType { get; set; }
        public string Issue { get; set; }
        public string Validity { get; set; }
        public string IssuedBy { get; set; }
        public int ApplyId { get; set; }
    }
}