﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class PersonIdentity
    {
        public string IdentityId { get; set; }
        public string Name { get; set; }
        public string Father { get; set; }
        public string Mother { get; set; }
        public string DateOfBirth { get; set; }
        public string Sex { get; set; }
        public string MobileNumber { get; set; }
        public string PermanentAddress { get; set; }
        public string PlaceOfBirth { get; set; }
        public string BloodGroup { get; set; }
        public string Type { get; set; }
        public string Nationality { get; set; }
        public byte[] Image { get; set; }



        public int CalculateAge(DateTime dateOfBirth)
        {
            DateTime now = DateTime.Now;
            int years = new DateTime(DateTime.Now.Subtract(dateOfBirth).Ticks).Year - 1;
            DateTime pastYearDate = dateOfBirth.AddYears(years);
            int Months = 0;
            for (int i = 1; i <= 12; i++)
            {
                if (pastYearDate.AddMonths(i) == now)
                {
                    Months = i;
                    break;
                }
                else if (pastYearDate.AddMonths(i) >= now)
                {
                    Months = i - 1;
                    break;
                }
            }
            return years;
        }




    }
}