﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class TestInfo
    {
        public int TestInfoId { get; set; }
        public int ApplyId { get; set; }
        public string TestType { get; set; }
        public string QAns1 { get; set; }
        public string QAns2 { get; set; }
        public string QAns3 { get; set; }
        public string QAns4 { get; set; }
        public string QAns5 { get; set; }
        public string TesterUsername { get; set; }
        public string Status { get; set; }
        public string TestDate { get; set; }
    }
}