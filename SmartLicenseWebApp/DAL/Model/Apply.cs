﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class Apply
    {
        public int ApplyId { get; set; }
        public string Username { get; set; }
        public string LicenseType { get; set; }
        public string ApplyFor { get; set; }
        public string Payment { get; set; }
        public string TestDate { get; set; }
        public string IsIssued { get; set; }
        public byte[] UtilityImage { get; set; }
        public byte[] DocumentImage { get; set; }

        public static string AmountForApplied(string applyFor, int amount)
        {
            int tempAmount;
            if (applyFor== "Two Wheels")
            {
                tempAmount = 4000;
            }else if (applyFor == "Four Wheels")
            {
                tempAmount = 8000;
            }
            else if (applyFor == "Three Wheels")
            {
                tempAmount = 5000;
            }
            else
            {
                tempAmount = 10000;
            }

            if (amount==tempAmount)
            {
                return "Success";
            }
            else
            {
                return "You Apply For "+applyFor+" And Payable Amount: "+tempAmount+"/- TK";
            }

        }

    }
}