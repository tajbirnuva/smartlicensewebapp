﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using SmartLicenseWebApp.BLL;

namespace SmartLicenseWebApp.DAL.Model
{
    public class Bkash
    {
        public string Number { get; set; }
        public string Pin { get; set; }
        public int Amount { get; set; }


        public static string PaymentStatus(Bkash aBkash)
        {
            Bkash bkash = new Manager().GetBkashInfo(aBkash.Number);
            if (bkash!=null)
            {
                if (bkash.Pin==aBkash.Pin)
                {
                    if (bkash.Amount>=aBkash.Amount)
                    {
                        int newAmmount = bkash.Amount - aBkash.Amount;
                        string message=new Manager().UpdateBkashAmount(bkash.Number,newAmmount);
                        //if need Add money method for brta account
                        if (message== "Save Successful")
                        {
                            return "Payment Successful";
                        }
                        else
                        {
                            return message;
                        }
                    }
                    else
                    {
                        return "Insufficient Balance!!!!!!!!!";
                    }
                    
                }
                else
                {
                    return "Pin Do Not Match!!!!!!";
                }
            }
            else
            {
                return "Bkash Number not Found!!!!!!!";
            }
        }



    }
}