﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class Offense
    {
        public int OffenseId { get; set; }
        public string LicenseId { get; set; }
        public string OffenseType { get; set; }
        public string IssuedBy { get; set; }
        public string IssueDate { get; set; }
        public string Status { get; set; }
    }
}