﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SmartLicenseWebApp.DAL.Model
{
    public class Users
    {
        public String Username { get; set; }
        public String Password { get; set; }
        public String IdentityId { get; set; }
        public String Status { get; set; }
    }
}